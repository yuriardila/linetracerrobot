/*****************************************************************************
 *
 *      ƒVƒXƒeƒ€–¼F@ƒ‰ƒCƒ“‚ÆƒŒ[ƒT[ƒƒ{ƒbƒg
 *
 *		Copyright (c) Yuri Ardila 2015
 *
 *-----------------------------------------------------------------------------
 *
 *		$RCSfile: line_tracer_robot.h,v $		$Revision: 1.0 $
 *
 *		$Ý@Œv: Yuri Ardila			$“ú•t: 2015/06/06
 *		$Author: Yuri Ardila $		$Date: 2015-06-06 17:08:02+09 $
 *
 *******************************************************************************/

#ifndef _LINE_TRACER_ROBOT_H
#define _LINE_TRACER_ROBOT_H

#include <limits.h>
#include "general.h"
#include "trainer.h"

/*
Preprocessors for robot movement type and state
*/

#define LTR_MAX_STATES 20

#define LTR_MIN_SPEED_PERIOD 5
#define LTR_MAX_SPEED_PERIOD 1

#define LTR_ACCEL_TIME 100
#define LTR_DECEL_TIME 100

/*
Typedefs
*/

/* Robot movement Enumeration */
enum RBT_MOV { 
	
	/* Stop movement */
	RBT_MOV_STP		 		= 0,
	
	/* Run with a certain speed */
	RBT_MOV_RUN_STRAIGHT    = 10,
    RBT_MOV_RUN_SPEED_1MS   = 11,
	RBT_MOV_RUN_SPEED_2MS	= 12,
	RBT_MOV_RUN_SPEED_3MS	= 13,
	RBT_MOV_RUN_SPEED_4MS	= 14,
	
	/*Turn left with a certain degree*/
    RBT_MOV_TRN_LEFT        = 20,
	RBT_MOV_TRN_LEFT_5DEG	= 21,
	RBT_MOV_TRN_LEFT_10DEG	= 22,
	RBT_MOV_TRN_LEFT_20DEG	= 23,
	RBT_MOV_TRN_LEFT_30DEG	= 24,

	/*Turn right with a certain degree*/
    RBT_MOV_TRN_RIGHT       = 30,
	RBT_MOV_TRN_RIGHT_5DEG	= 31,
	RBT_MOV_TRN_RIGHT_10DEG	= 32,
	RBT_MOV_TRN_RIGHT_20DEG	= 33,
	RBT_MOV_TRN_RIGHT_30DEG	= 34,

    /* Shutdown */
    RBT_MOV_SOFT_SHUTDOWN   = 101,
    RBT_MOV_HARD_SHUTDOWN   = 102,

	RBT_MOV_UNKNOWN 		= 99
};

/*Robot sensor Enumeration*/
enum RBT_SNS {  

    /*Mid: 1001*/
    RBT_SNS_MIDDLE          = 9,

    /*S-Left: 1011*/
    RBT_SNS_S_LEFT          = 11,
    /*M-Left: 0011*/
    RBT_SNS_M_LEFT          = 3,
    /*L-Left: 0111*/
    RBT_SNS_L_LEFT          = 7,

    /*S-Right: 1101*/
    RBT_SNS_S_RIGHT         = 13,
    /*M-Right: 1100*/
    RBT_SNS_M_RIGHT         = 12,
    /*L-Right: 1110*/
    RBT_SNS_L_RIGHT         = 14,

    /*Out of line: 1111*/
    RBT_SNS_OFFLINE         = 15,

    /*Crossing line: 0000*/
    RBT_SNS_CROSSLINE       = 0,

    RBT_SNS_UNKNOWN         = 99
};

/*
 *   rbt_movement_state         : ロボットの動作状態
 *   rbt_sensor_state           : ロボットのセンサー状態
 *   rbt_speed_timer_l_wheel    : ロボットの左タイヤの周期
 *   rbt_speed_timer_r_wheel    : ロボットの右タイヤの周期
 *   rbt_period_ms_ctr          : 現状態の1ms経過カウンタ
 */
typedef struct st_line_tracer_state_t {
    
    /* Robot states */
    uint8_t rbt_movement_state;
    uint8_t rbt_sensor_state;

    /* Robot current timer */
    uint8_t rbt_speed_timer_l_wheel;
    uint8_t rbt_speed_timer_r_wheel;

    /* Robot current period counter */
    unsigned int rbt_period_ms_ctr;

} st_line_tracer_state_t;

/* Robot Errors */
enum RBT_ERR { 
    
    /* Out of line */
    RBT_ERR_OOL             = 101,

    /* Unknown error */
    RBT_ERR_UNKNOWN         = 9999

};

/* 
List of Global Functions
*/

/* Robot interface */
void f_init_robot(void);
void f_line_tracer_robot(void);

/* State related functions */
void f_init_state(st_line_tracer_state_t* state);
bool_t f_predict_next_state(st_line_tracer_state_t *l_line_tracer_cur_state,
                            st_line_tracer_state_t *l_line_tracer_nxt_state,
                            st_line_tracer_state_t *l_line_tracer_prv_state,
                            uint8_t nxt_sensor);
void f_change_state(st_line_tracer_state_t **l_line_tracer_cur_state,
                    st_line_tracer_state_t **l_line_tracer_nxt_state,
                    st_line_tracer_state_t **l_line_tracer_prv_state);
void f_done_state(st_line_tracer_state_t* state);
st_line_tracer_state_t* f_get_current_state();
st_line_tracer_state_t* f_get_previous_state();
st_line_tracer_state_t* f_get_next_state();

/* 
List of Static Functions
*/

/* Movement related functions: Run */
static void f_run_accelerate(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state);
static void f_run_decelerate(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state);
static void f_run_straight(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state);

/* Movement related functions: Turn Degree */
static void f_turn_left_5deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state);
static void f_turn_left_10deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state);
static void f_turn_left_20deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state);

static void f_turn_right_5deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state);
static void f_turn_right_10deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state);
static void f_turn_right_20deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state);

static void f_do_nothing();

/* Sensor related functions */
static uint8_t f_is_middle(st_line_tracer_state_t* state);
static uint8_t f_is_s_left(st_line_tracer_state_t* state);
static uint8_t f_is_m_left(st_line_tracer_state_t* state);
static uint8_t f_is_l_left(st_line_tracer_state_t* state);
static uint8_t f_is_s_right(st_line_tracer_state_t* state);
static uint8_t f_is_m_right(st_line_tracer_state_t* state);
static uint8_t f_is_l_right(st_line_tracer_state_t* state);

/* Shutdown */
static void f_hard_shutdown(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state);
static void f_soft_shutdown(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state);

/* Misc */
void delay_ms(unsigned int x);				/* ソフトウェアディレイ */
uint8_t rotate_l(uint8_t data);				/* パターン左回転 */
uint8_t rotate_r(uint8_t data);             /* パターン右回転 */

#endif 
/* _LINE_TRACER_ROBOT_H */


