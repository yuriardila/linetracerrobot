/*******************************************************************************
 *
 *		システム名:	ＭＮ１０１Ｃ４９Ｋ特殊レジスタ定義ファイル（Ｃ言語）
 *
 *		Copyright (c) Panasonic Corporation 2003-2014
 *
 *------------------------------------------------------------------------------
 *
 *		$RCSfile: mn101c49.h,v $			$Revision: 1.1 $
 *
 *		$設　計: 森本泰司				$日付: 2003/ 7/16
 *		$Author: Y.Morimoto $			$Date: 2014-08-28 18:55:11+09 $
 *
 *******************************************************************************/

#ifndef _MN101C49K_H
#define	_MN101C49K_H

#include "general.h"

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		ＣＰＵモード・メモリ制御
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define CPUM     ((volatile unsigned char *)0x3F00) /* CPUモード制御レジスタ */
#define MEMCTR   ((volatile unsigned char *)0x3F01) /* メモリ制御レジスタ */
#define WDCTR    ((volatile unsigned char *)0x3F02) /* ウォッチドッグタイマ制御レジスタ */
#define DLYCTR   ((volatile unsigned char *)0x3F03) /* 発振安定待ち制御レジスタ */
#define ACTMD    ((volatile unsigned char *)0x3F06) /* ACタイミング制御レジスタ */
#define RCCTR    ((volatile unsigned char *)0x3F09) /* ROMコレクション制御レジスタ */
#define SBNKR    ((volatile unsigned char *)0x3F0A) /* ソースアドレス用バンクレジスタ */
#define DBNKR    ((volatile unsigned char *)0x3F0B) /* ディスティネーションアドレス用レジスタ */
#define OSCMD    ((volatile unsigned char *)0x3F0D) /* 発信周波数制御レジスタ */
#define EXADV    ((volatile unsigned char *)0x3F0E) /* 拡張アドレス制御レジスタ */

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		ポート出力
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define P0OUT	((volatile unsigned char *)0x3F10)
#define P1OUT	((volatile unsigned char *)0x3F11)
#define P2OUT	((volatile unsigned char *)0x3F12)	/* P27:ソフトリセット */
#define P3OUT	((volatile unsigned char *)0x3F13)
#define P4OUT	((volatile unsigned char *)0x3F14)
#define P5OUT	((volatile unsigned char *)0x3F15)
#define P6OUT	((volatile unsigned char *)0x3F16)
#define P7OUT	((volatile unsigned char *)0x3F17)
#define P8OUT	((volatile unsigned char *)0x3F18)
#define PCOUT	((volatile unsigned char *)0x3F1C)
#define PDOUT	((volatile unsigned char *)0x3F1D)

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		ポート入力
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define P0IN	((const volatile unsigned char *)0x3F20)
#define P1IN	((const volatile unsigned char *)0x3F21)
#define P2IN	((const volatile unsigned char *)0x3F22)
#define P3IN	((const volatile unsigned char *)0x3F23)
#define P4IN	((const volatile unsigned char *)0x3F24)
#define P5IN	((const volatile unsigned char *)0x3F25)
#define P6IN	((const volatile unsigned char *)0x3F26)
#define P7IN	((const volatile unsigned char *)0x3F27)
#define P8IN	((const volatile unsigned char *)0x3F28)
#define PAIN	((const volatile unsigned char *)0x3F2A)
#define PCIN	((const volatile unsigned char *)0x3F2C)
#define PDIN	((const volatile unsigned char *)0x3F2D)

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		入出力モード制御
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define P0DIR	((volatile unsigned char *)0x3F30)
#define P1DIR	((volatile unsigned char *)0x3F31)
#define P3DIR	((volatile unsigned char *)0x3F33)
#define P4DIR	((volatile unsigned char *)0x3F34)
#define P5DIR	((volatile unsigned char *)0x3F35)
#define P6DIR	((volatile unsigned char *)0x3F36)
#define P7DIR	((volatile unsigned char *)0x3F37)
#define P8DIR	((volatile unsigned char *)0x3F38)
#define PAIMD	((volatile unsigned char *)0x3F3A)
#define PCDIR	((volatile unsigned char *)0x3F3C)
#define PDDIR	((volatile unsigned char *)0x3F3D)
#define P4IMD	((volatile unsigned char *)0x3F3E)

#define PDSYO	((volatile unsigned char *)0x3F1F)
#define P1OMD	((volatile unsigned char *)0x3F2F)
#define P1TCNT	((volatile unsigned char *)0x3F7E)	/* リアルタイム出力制御 */

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		Ｉ／Ｏポート抵抗制御
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define P0PLU	((volatile unsigned char *)0x3F40)
#define P1PLU	((volatile unsigned char *)0x3F41)
#define P2PLU	((volatile unsigned char *)0x3F42)
#define P3PLU	((volatile unsigned char *)0x3F43)
#define P4PLUD	((volatile unsigned char *)0x3F44)
#define P5PLU	((volatile unsigned char *)0x3F45)
#define P6PLU	((volatile unsigned char *)0x3F46)
#define P7PLUD	((volatile unsigned char *)0x3F47)
#define P8PLU	((volatile unsigned char *)0x3F48)
#define PAPLUD	((volatile unsigned char *)0x3F4A)
#define PCPLU	((volatile unsigned char *)0x3F4C)
#define PDPLU	((volatile unsigned char *)0x3F4D)

#define FLOAT	((volatile unsigned char *)0x3F2E)

#define SYOEV0	BIT_POSITION_0
#define SYOEV1	BIT_POSITION_1
#define P4RDWN	BIT_POSITION_3
#define P7RDWN	BIT_POSITION_4
#define PARDWN	BIT_POSITION_6

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		タイマー制御
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define TM0BC	((const volatile unsigned char *)0x3F50)
#define TM1BC	((const volatile unsigned char *)0x3F51)
#define TM0OC	((volatile unsigned char *)0x3F52)
#define TM1OC	((volatile unsigned char *)0x3F53)
#define TM0MD	((volatile unsigned char *)0x3F54)
#define TM1MD	((volatile unsigned char *)0x3F55)
#define CK0MD	((volatile unsigned char *)0x3F56)
#define CK1MD	((volatile unsigned char *)0x3F57)
#define TM2BC	((const volatile unsigned char *)0x3F58)
#define TM3BC	((const volatile unsigned char *)0x3F59)
#define TM2OC	((volatile unsigned char *)0x3F5A)
#define TM3OC	((volatile unsigned char *)0x3F5B)
#define TM2MD	((volatile unsigned char *)0x3F5C)
#define TM3MD	((volatile unsigned char *)0x3F5D)
#define CK2MD	((volatile unsigned char *)0x3F5E)
#define CK3MD	((volatile unsigned char *)0x3F5F)

#define TM4BC	((const volatile unsigned char *)0x3F60)
#define TM4OC	((volatile unsigned char *)0x3F62)
#define TM4MD	((volatile unsigned char *)0x3F64)
#define CK4MD	((volatile unsigned char *)0x3F66)
#define TM6BC	((const volatile unsigned char *)0x3F68)
#define TM6OC	((volatile unsigned char *)0x3F69)
#define TM6MD	((volatile unsigned char *)0x3F6A)
#define TBCLR	((volatile unsigned char *)0x3F6B)
#define RMCTR	((volatile unsigned char *)0x3F6E)
#define PSCMD	((volatile unsigned char *)0x3F6F)

#define TM7BCL	((const volatile unsigned char *)0x3F70)
#define TM7BCH	((const volatile unsigned char *)0x3F71)
#define TM7OC1L	((const volatile unsigned char *)0x3F72)
#define TM7OC1H	((const volatile unsigned char *)0x3F73)
#define TM7PR1L	((volatile unsigned char *)0x3F74)
#define TM7PR1H	((volatile unsigned char *)0x3F75)
#define TM7ICL	((const volatile unsigned char *)0x3F76)
#define TM7ICH	((const volatile unsigned char *)0x3F77)
#define TM7MD1	((volatile unsigned char *)0x3F78)
#define TM7MD2	((volatile unsigned char *)0x3F79)
#define TM7OC2L	((const volatile unsigned char *)0x3F7A)
#define TM7OC2H	((const volatile unsigned char *)0x3F7B)
#define TM7PR2L	((volatile unsigned char *)0x3F7C)
#define TM7PR2H	((volatile unsigned char *)0x3F7D)

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		シリアルインタフェース制御
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define SC0MD0	((volatile unsigned char *)0x3F90)
#define SC0MD1	((volatile unsigned char *)0x3F91)
#define SC0MD2	((volatile unsigned char *)0x3F92)
#define SC0STR	((const volatile unsigned char *)0x3F93)
#define RXBUF0	((const volatile unsigned char *)0x3F94)
#define TXBUF0	((volatile unsigned char *)0x3F95)
#define SC0ODC	((volatile unsigned char *)0x3F96)
#define SC0CKS	((volatile unsigned char *)0x3F97)
#define RXBUF1	((const volatile unsigned char *)0x3F98)
#define TXBUF1	((volatile unsigned char *)0x3F99)
#define SC1MD0	((volatile unsigned char *)0x3F9A)
#define SC1MD1	((volatile unsigned char *)0x3F9B)
#define SC1MD2	((volatile unsigned char *)0x3F9C)
#define SC1STR	((const volatile unsigned char *)0x3F9D)
#define SC1ODC	((volatile unsigned char *)0x3F9E)
#define SC1CKS	((volatile unsigned char *)0x3F9F)

#define SC2MD0	((volatile unsigned char *)0x3FA0)
#define SC2MD1	((volatile unsigned char *)0x3FA1)
#define SC2TRB	((volatile unsigned char *)0x3FA2)
#define SC2ODC	((volatile unsigned char *)0x3FA6)
#define SC2CKS	((volatile unsigned char *)0x3FA7)
#define SC3MD0	((volatile unsigned char *)0x3FA8)
#define SC3MD1	((volatile unsigned char *)0x3FA9)
#define SC3CTR	((volatile unsigned char *)0x3FAA)
#define SC3TRB	((volatile unsigned char *)0x3FAB)
#define SC3ODC	((volatile unsigned char *)0x3FAE)
#define SC3CKS	((volatile unsigned char *)0x3FAF)

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		Ａ／Ｄ、Ｄ／Ａ制御
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define ANCTR0	((volatile unsigned char *)0x3FB0)

#define ANCTR1	((volatile unsigned char *)0x3FB1)
#define ANCTR2	((volatile unsigned char *)0x3FB2)
#define	ANST	BIT_POSITION_7							/* A/D変換スタート */

#define ANBUF0	((const volatile unsigned char *)0x3FB3)
#define ANBUF1	((const volatile unsigned char *)0x3FB4)

#define DACTR	((volatile unsigned char *)0x3FBB)
#define DADR0	((volatile unsigned char *)0x3FBC)
#define DADR1	((volatile unsigned char *)0x3FBD)
#define DADR2	((volatile unsigned char *)0x3FBE)
#define DADR3	((volatile unsigned char *)0x3FBF)

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		ＲＯＭコレクション制御
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define RC0APL	((volatile unsigned char *)0x3FC7)
#define RC0APM	((volatile unsigned char *)0x3FC8)
#define RC0APH	((volatile unsigned char *)0x3FC9)
#define RC1APL	((volatile unsigned char *)0x3FCA)
#define RC1APM	((volatile unsigned char *)0x3FCB)
#define RC1APH	((volatile unsigned char *)0x3FCC)
#define RC2APL	((volatile unsigned char *)0x3FCD)
#define RC2APM	((volatile unsigned char *)0x3FCE)
#define RC2APH	((volatile unsigned char *)0x3FCF)

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		ＡＴＣ制御
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define AT1CNT0		((volatile unsigned char *)0x3FD0)
#define AT1CNT1		((volatile unsigned char *)0x3FD1)
#define AT1TRC		((volatile unsigned char *)0x3FD2)
#define AT1MAP0L	((volatile unsigned char *)0x3FD3)
#define AT1MAP0M	((volatile unsigned char *)0x3FD4)
#define AT1MAP0H	((volatile unsigned char *)0x3FD5)
#define AT1MAP1L	((volatile unsigned char *)0x3FD6)
#define AT1MAP1M	((volatile unsigned char *)0x3FD7)
#define AT1MAP1H	((volatile unsigned char *)0x3FD8)

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *		割込み制御
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#define NMICR	((volatile unsigned char *)0x3FE1)
#define IRQ0ICR	((volatile unsigned char *)0x3FE2)
#define IRQ1ICR	((volatile unsigned char *)0x3FE3)
#define IRQ2ICR	((volatile unsigned char *)0x3FE4)
#define IRQ3ICR	((volatile unsigned char *)0x3FE5)
#define IRQ4ICR	((volatile unsigned char *)0x3FE6)
#define IRQ5ICR	((volatile unsigned char *)0x3FE7)
#define TM0ICR	((volatile unsigned char *)0x3FE9)
#define TM1ICR	((volatile unsigned char *)0x3FEA)
#define TM2ICR	((volatile unsigned char *)0x3FEB)
#define TM3ICR	((volatile unsigned char *)0x3FEC)
#define TM4ICR	((volatile unsigned char *)0x3FED)
#define TM6ICR	((volatile unsigned char *)0x3FEF)

#define TBICR		((volatile unsigned char *)0x3FF0)
#define TM7ICR		((volatile unsigned char *)0x3FF1)
#define T7OC2ICR	((volatile unsigned char *)0x3FF2)
#define SC0RICR		((volatile unsigned char *)0x3FF5)
#define SC0TICR		((volatile unsigned char *)0x3FF6)
#define SC1ICR		((volatile unsigned char *)0x3FF7)
#define SC2ICR		((volatile unsigned char *)0x3FF8)
#define SC3ICR		((volatile unsigned char *)0x3FF9)
#define ADICR		((volatile unsigned char *)0x3FFA)
#define ATC1ICR		((volatile unsigned char *)0x3FFC)

#define NFCTR	((volatile unsigned char *)0x3F8E)
#define EDGDT	((volatile unsigned char *)0x3F8F)


#define ei()	asm("	or	0x70,PSW\n")	/* Fill in the blanks! */
#define di()	asm("	and	0xbf,PSW\n")

#define inp(iop)		(*(iop))
#define outp(iop, data)	(*(iop) = (data))

#endif	/* _MN101C49K_H */

/*------------------ end of mn101c49.h ----------------------------------------*/
