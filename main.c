/****************************************************************************
 *
 *		システム名：　ライントレーサーロボット
 *
 *		Copyright (c) Yuri Ardila 2015
 *
 *------------------------------------------------------------------------------
 *
 *		$RCSfile: main.c,v $		$Revision: 1.0 $
 *
 *		$設　計: Yuri Ardila			$日付: 2015/06/04
 *		$Author: Yuri Ardila $		$Date: 2015-06-04 18:36:45+09 $
 *
 *******************************************************************************/

#include <limits.h>
#include "general.h"
#include "trainer.h"
#include "line_tracer_robot.h"

static const char rcsid[] = "$Id: main.c,v 1.1 2014-08-29 18:36:45+09 Y.Morimoto Exp $";

int error_no;

/* Unit Tests: ユニットテストを行う際、下のプリプロセッサー(UNITTEST)をコメントアウトする */
/*#define UNITTEST*/
#ifdef UNITTEST

#define UNITTEST_LTR_MOVE_L_WHEEL
#define UNITTEST_LTR_MOVE_L_WHEEL_WITH_PERIOD
#define UNITTEST_LTR_MOVE_R_WHEEL
#define UNITTEST_LTR_MOVE_R_WHEEL_WITH_PERIOD
#define UNITTEST_LTR_MOVE_BOTH_WHEEL
#define UNITTEST_LTR_MOVE_BOTH_WHEEL_WITH_PERIOD
#define UNITTEST_LTR_MOVE_ON_SNS_MIDDLE
#define UNITTEST_LTR_MOVE_ON_SNS_S_LEFT
#define UNITTEST_LTR_MOVE_ON_SNS_M_LEFT
#define UNITTEST_LTR_MOVE_ON_SNS_L_LEFT
#define UNITTEST_LTR_CHECK_LCD
#define UNITTEST_LTR_CHECK_LCD_COUNTER_1S
#define UNITTEST_LTR_CHECK_SENSOR

#define UNITTEST_LTR_PREDICT_NEXT_STATE_BY_PREDEF
#define UNITTEST_LTR_PREDICT_NEXT_STATE_BY_SENSOR

#include "unittest_line_tracer_robot.h"

#endif 
/* Unit Tests */

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *	関数名 :    main	システムのメインプログラム
 *
 *	書　式 :    void main(void)
 *
 *	機　能 :    
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void main(void)
{
#ifdef UNITTEST_LTR_PREDICT_NEXT_STATE_BY_PREDEF
	st_line_tracer_state_t cur_state;
	uint8_t nxt_sensor;
#endif

	int err_lcd = 0;						/* LCD制御でのエラー番号 */
    unsigned char lcd0_written_flag = 0;
    unsigned char lcd1_written_flag = 0;
    
	init_lcd();

	ei();									/* 割込み許可 */

#ifdef UNITTEST_LTR_MOVE_L_WHEEL
	unittest_ltr_move_l_wheel(); 
#endif

#ifdef UNITTEST_LTR_MOVE_L_WHEEL_WITH_PERIOD
	unittest_ltr_move_l_wheel_with_period(5); 
	unittest_ltr_move_l_wheel_with_period(4); 
	unittest_ltr_move_l_wheel_with_period(3); 
	unittest_ltr_move_l_wheel_with_period(2); 
	unittest_ltr_move_l_wheel_with_period(1); 
#endif

#ifdef UNITTEST_LTR_MOVE_R_WHEEL
	unittest_ltr_move_r_wheel(); 
#endif

#ifdef UNITTEST_LTR_MOVE_R_WHEEL_WITH_PERIOD
	unittest_ltr_move_r_wheel_with_period(2); 
#endif

#ifdef UNITTEST_LTR_MOVE_BOTH_WHEEL
	unittest_ltr_move_both_wheel(); 
#endif

#ifdef UNITTEST_LTR_MOVE_BOTH_WHEEL_WITH_PERIOD
	unittest_ltr_move_both_wheel_with_period(2); 
#endif

#ifdef UNITTEST_LTR_MOVE_ON_SNS_MIDDLE
	unittest_ltr_move_on_sns_middle(); 
#endif

#ifdef UNITTEST_LTR_MOVE_ON_SNS_S_LEFT
	unittest_ltr_move_on_sns_s_left(); 
#endif

#ifdef UNITTEST_LTR_MOVE_ON_SNS_M_LEFT
	unittest_ltr_move_on_sns_m_left(); 
#endif

#ifdef UNITTEST_LTR_MOVE_ON_SNS_L_LEFT
	unittest_ltr_move_on_sns_l_left(); 
#endif

#ifdef UNITTEST_LTR_CHECK_LCD
	unittest_ltr_check_lcd("Testing LCD...");
#endif

#ifdef UNITTEST_LTR_CHECK_LCD_COUNTER_1S
	unittest_ltr_check_lcd_counter_1s();
#endif

#ifdef UNITTEST_LTR_CHECK_SENSOR
	unittest_ltr_check_sensor();
#endif

#ifdef UNITTEST_LTR_PREDICT_NEXT_STATE_BY_PREDEF
	cur_state.rbt_movement_state = RBT_MOV_STP;
	cur_state.rbt_sensor_state = RBT_SNS_UNKNOWN;
	cur_state.rbt_speed_timer_l_wheel = 0;
	cur_state.rbt_speed_timer_r_wheel = 0;
	cur_state.rbt_period_ms_ctr = 0;
	nxt_sensor = RBT_SNS_MIDDLE;
	unittest_ltr_predict_next_state_by_predef(&cur_state, nxt_sensor);
#endif

#ifdef UNITTEST_LTR_PREDICT_NEXT_STATE_BY_SENSOR
	unittest_ltr_predict_next_state_by_sensor();
#endif

	/* ロボット初期化 */
	f_init_robot();

	/* ライントレースロボット */
	f_line_tracer_robot();

	return;
}
/*------------------ end of main() --------------------------------------------*/
/*------------------ end of main.c --------------------------------------------*/