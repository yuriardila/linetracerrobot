/*******************************************************************************
 *
 *      システム名：　ラインとレーサーロボット
 *
 *		Copyright (c) Yuri Ardila 2015
 *
 *-----------------------------------------------------------------------------
 *
 *		$RCSfile: line_tracer_robot_constant.h,v $		$Revision: 1.0 $
 *
 *		$設　計: Yuri Ardila			$日付: 2015/06/06
 *		$Author: Yuri Ardila $		$Date: 2015-06-06 17:08:02+09 $
 *
 *******************************************************************************/

#ifndef _LINE_TRACER_ROBOT_CONSTANT_H
#define _LINE_TRACER_ROBOT_CONSTANT_H

#define LINE_TRACER_MAX_SENSOR_COUNTER 10
#define LINE_TRACER_MIN_SENSOR_COUNTER 1

#define LINE_TRACER_MAX_PULSE_COUNTER 100
#define LINE_TRACER_MIN_PULSE_COUNTER 10

#define LINE_TRACER_CURRENT_STATE_COUNTER 10

#endif /* _LINE_TRACER_ROBOT_CONSTANT_H */