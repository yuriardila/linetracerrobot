/*******************************************************************************
 *
 *      システム名：　割込みルーチン（MN101C49K用）ラインとレーサーロボット（割込み）
 *
 *		Copyright (c) Panasonic Corporation 2003-2014
 *
 *-----------------------------------------------------------------------------
 *
 *		$RCSfile: interrupt.c,v $		$Revision: 1.3 $
 *
 *		$設　計: 森本泰司			$日付: 2001/11/ 6
 *		$Author: Y.Morimoto $		$Date: 2014-11-03 17:08:02+09 $
 *
 *******************************************************************************/

#include "general.h"
#include "trainer.h"

static unsigned int over_1s;            /* 1s経過カウンタ */
static unsigned int over_1ms;           /* 1ms経過カウンタ */
static unsigned int over_2ms;           /* 2ms経過カウンタ */
static unsigned int over_3ms;           /* 3ms経過カウンタ */
static unsigned int over_4ms;           /* 4ms経過カウンタ */
static unsigned int over_5ms;           /* 5ms経過カウンタ */
static unsigned int over_10ms;          /* 10ms経過カウンタ */

static volatile unsigned int is_fix;    /* バッファリング完了フラグ */
static volatile unsigned int is_fix_mid;/* Buffering for middle only */
static volatile unsigned int is_fix_off;/* Buffering for offline only */
static volatile unsigned int is_fix_tsw;/* Buffering for tsw */

static unsigned char tsw;               /* バッファリングデータ：TSW */
static unsigned char sns;               /* バッファリングデータ：SENSOR */

/*---------------------------------------------------------- EJECT ----*/
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   tmint_1ms      1ms基準タイマ
 *
 *  書　式 :   void tmint_1ms(void)
 *
 *  機　能 :   1. 1ms毎に入力のバッファリング処理を行う
 *            2. 1ms毎に1ms, 2ms, 3ms, 4ms, 5ms, 10ms, 1s経過カウンタをインクリメントする
 *            3. 2s毎に、ロボットの位置が線の中央、若しくは線から外れているかのチェックを行う
 *
 *  結　果 :   ファイルスタティック変数：unsigned char sns, unsigned char tsw
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#ifdef __CC101__
#pragma _interrupt tmint_1ms
#endif
void tmint_1ms(void)
{
    static unsigned char buf_sns, buf_tsw;
    unsigned char buf, buf2;

    /* Take sensor input */
    buf = inp(SNS);
    buf &= 0x0f;

    /* 1ms経過カウンタ */
    over_1ms++;

    /* 2ms経過カウンタ */
    if (over_1ms % 2 == 0 && over_1ms != 0) {
        over_2ms++;
    } 

    /* 3ms経過カウンタ */
    if (over_1ms % 3 == 0 && over_1ms != 0) {
        over_3ms++;
    } 

    /* 4ms経過カウンタ */
    if (over_1ms % 4 == 0 && over_1ms != 0) {
        over_4ms++;
    } 

    /* 5ms経過カウンタ */
    if (over_1ms % 5 == 0 && over_1ms != 0) {
        over_5ms++;
    } 

    /* 10ms経過カウンタ */
    if (over_1ms % 10 == 0 && over_1ms != 0) {
        over_10ms++;

        /* Take TSW input */
        buf2 = inp(TSW);
        if (buf2 == buf_tsw) {
            tsw = buf_tsw;
            is_fix_tsw |= BIT_PATTERN_0;
        }
        buf_tsw = buf2;
    }                    

    /* 1s経過カウンタ */
    if (over_1ms % 1000 == 0 && over_1ms != 0) {
        over_1s++;
    }   

    /* 2s経過 */
    if (over_1ms % 2000 == 0 && over_1ms != 0) {
        
        /* wait until it is fixed on middle position (0x09) */
        if (buf == buf_sns && buf == 0x09) {
            is_fix_mid |= BIT_PATTERN_0;
        }

        /* Take sensor input, and wait until it is fixed on offline (0x0f) */
        if (buf == buf_sns && buf == 0x0f) {
            is_fix_off |= BIT_PATTERN_0;
        }
    }  

    /* Take sensor input */
    if (buf == buf_sns) {
        sns = buf_sns;
        is_fix |= BIT_PATTERN_0;
    }

    buf_sns = buf;  
}
/*------------------ end of tmint_10ms() ------------------- EJECT ----*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *      関数名 :    get_time_1s       経過時間の取得
 *
 *      書　式 :    unsigned int get_time_1s(void)
 *
 *      機　能 :    1s経過カウンタの値を返す
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

unsigned int get_time_1s(void)
{
    return over_1s;
}
/*------------------ end of get_time_1s() -------------------------------*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *      関数名 :    get_time_10ms       経過時間の取得
 *
 *      書　式 :    unsigned int get_time_10ms(void)
 *
 *      機　能 :    10ms経過カウンタの値を返す
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

unsigned int get_time_10ms(void)
{
    return over_10ms;
}
/*------------------ end of get_time_10ms() -------------------------------*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *      関数名 :    get_time_5ms       経過時間の取得
 *
 *      書　式 :    unsigned int get_time_5ms(void)
 *
 *      機　能 :    5ms経過カウンタの値を返す
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

unsigned int get_time_5ms(void)
{
    return over_5ms;
}
/*------------------ end of get_time_5ms() -------------------------------*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *      関数名 :    get_time_4ms       経過時間の取得
 *
 *      書　式 :    unsigned int get_time_4ms(void)
 *
 *      機　能 :    4ms経過カウンタの値を返す
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

unsigned int get_time_4ms(void)
{
    return over_4ms;
}
/*------------------ end of get_time_4ms() -------------------------------*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *      関数名 :    get_time_3ms       経過時間の取得
 *
 *      書　式 :    unsigned int get_time_3ms(void)
 *
 *      機　能 :    3ms経過カウンタの値を返す
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

unsigned int get_time_3ms(void)
{
    return over_3ms;
}
/*------------------ end of get_time_3ms() -------------------------------*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *      関数名 :    get_time_2ms       経過時間の取得
 *
 *      書　式 :    unsigned int get_time_2ms(void)
 *
 *      機　能 :    2ms経過カウンタの値を返す
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

unsigned int get_time_2ms(void)
{
    return over_2ms;
}
/*------------------ end of get_time_2ms() -------------------------------*/


/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *      関数名 :    get_time_1ms       経過時間の取得
 *
 *      書　式 :    unsigned int get_time_1ms(void)
 *
 *      機　能 :    1ms経過カウンタの値を返す
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

unsigned int get_time_1ms(void)
{
    return over_1ms;
}
/*------------------ end of get_time_1ms() -------------------------------*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *      関数名 :    reset_timers       経過時間をリセット
 *
 *      書　式 :    unsigned void reset_timers(void)
 *
 *      機　能 :    経過時間をリセット
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void reset_timers(void)
{
    over_1ms = 0;
    over_2ms = 0;
    over_3ms = 0;
    over_4ms = 0;
    over_5ms = 0;
    over_10ms = 0;
    over_1s = 0;
}
/*------------------ end of get_time_1ms() -------------------------------*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   get_tsw     センサー入力
 *
 *  書　式 :   unsigned char get_tsw(void)
 *
 *  機　能 :   1.バッファリングデータtswを返す
 *
 *  結　果 :   戻り値：tsw
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

unsigned char get_tsw(void)
{
    return tsw;
}
/*------------------ end of get_tsw() ---------------------------------*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   get_sns     センサー入力
 *
 *  書　式 :   unsigned char get_sns(void)
 *
 *  機　能 :   バッファリングデータsnsを返す
 *
 *  結　果 :   戻り値：sns
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

unsigned char get_sns(void)
{
    return sns;
}
/*------------------ end of get_sns() ---------------------------------*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *      関数名 :    wait_fix       バッファリング処理待ち
 *
 *      書　式 :    void wait_fix(void)
 *
 *      機　能 :    センサー入力バッファリング処理が終わるのを待つ
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void wait_fix(void)
{
    while (is_fix != BIT_PATTERN_0)    /* バッファリング完了待ち */
        ;
}
/*------------------ end of wait_fix() --------------------- EJECT ----*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *      関数名 :    wait_fix_tsw       バッファリング処理待ち
 *
 *      書　式 :    void wait_fix_tsw(void)
 *
 *      機　能 :    トグルスイッチの最初の入力バッファリング処理が終わるのを待つ
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void wait_fix_tsw(void)
{
    while (is_fix_tsw != BIT_PATTERN_0)    /* バッファリング完了待ち */
        ;
}
/*------------------ end of wait_fix_tsw() --------------------- EJECT ----*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *      関数名 :    wait_fix_mid       バッファリング処理待ち
 *
 *      書　式 :    void wait_fix_mid(void)
 *
 *      機　能 :    センサー入力が中央になるまで、バッファリング処理が終わるのを待つ
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void wait_fix_mid(void)
{
    while (is_fix_mid != BIT_PATTERN_0)    /* バッファリング完了待ち */
        ;
}
/*------------------ end of wait_fix_mid() --------------------- EJECT ----*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *      関数名 :    set_fix_mid_0       is_fix_midをリセット
 *
 *      書　式 :    void set_fix_mid_0(void)
 *
 *      機　能 :    is_fix_midをリセット
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void set_fix_mid_0(void)
{
    is_fix_mid = 0;
}
/*------------------ end of set_fix_mid_0() --------------------- EJECT ----*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *      関数名 :    get_fix_off       is_fix_offを取得
 *
 *      書　式 :    void get_fix_off(void)
 *
 *      機　能 :    is_fix_offの値を取得
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

unsigned int get_fix_off(void)
{
    return is_fix_off;
}
/*------------------ end of get_fix_off() -------------------------*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *      関数名 :    set_fix_off_0       is_fix_offをリセット
 *
 *      書　式 :    void set_fix_off_0(void)
 *
 *      機　能 :    is_fix_offをリセット
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void set_fix_off_0(void)
{
    is_fix_off = 0;
}
/*------------------ end of set_fix_off_0() --------------------- EJECT ----*/
/*------------------ end of interrupt.c ---------------------------------------*/
