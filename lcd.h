/**********************************************************************
 *
 *		システム名：　スタータキットLCD用ヘッダファイル （SC1602BSxB用）
 *
 *		Copyright (c) 2003 松下電器産業株式会社　ソフト・技術研修グループ
 *
 *---------------------------------------------------------------------
 *
 *		$RCSfile: lcd.h,v $		$Revision: 1.2 $
 *
 *		$設　計: 森本泰司			$日付: 2003/ 7/17
 *		$Author: Y.Morimoto $		$Date: 2007-03-17 13:20:51+09 $
 *
 **********************************************************************/

#ifndef _LCD_H
#define _LCD_H

#include "general.h"

/* 関数プロトタイプ宣言 */

void init_lcd(void);
int lcd_putchar(int c);
int lcd_puts(const char *s);
int lcd_puts_line_loc(const char *s, unsigned char line, unsigned char cursor);
int ctrl_lcd0(void);
int ctrl_lcd1(void);
void reset_lcdbuf(void);

#define LCD_X_SIZE		(16)		/* SC1602BSxB */
#define LCD_Y_SIZE		(2)

#define LCD_WRITE		(0<<0)		/* LCD_RW_CTRL */
#define LCD_READ		(1<<0)
#define LCD_EN			(1<<2)

#define LCD_CMD_SEL		(0<<0)		/* LCD_RS_SEL */
#define LCD_DATA_SEL	(1<<0)

#define LCD_CLR_DSP		(1<<0)		/* 表示クリア */

#define LCD_HOME_CUR	(1<<1)		/* カーソルホーム */

#define LCD_EMODE		(1<<2)		/* エントリーモードセット */
#define LCD_EMODE_INC	(1<<1)			/* アドレスインクリメント */
#define LCD_EMODE_DEC	(0<<1)			/* アドレスデクリメント */
#define LCD_EMODE_SHIFT	(1<<0)			/* 表示シフトする */
#define LCD_EMODE_NOSFT	(0<<0)			/* 表示シフトしない */

#define LCD_DSP			(1<<3)		/* 表示オン／オフコントロール */
#define LCD_DSP_ON		(1<<2)
#define LCD_DSP_OFF		(0<<2)
#define LCD_DSP_CUR_ON	(1<<1)
#define LCD_DSP_CUR_OFF	(0<<1)
#define LCD_DSP_BLINK	(1<<0)
#define LCD_DSP_NOBLINK	(0<<0)

#define LCD_SHIFT		(1<<4)		/* カーソル／表示シフト */
#define LCD_SHIFT_L_CUR	(0<<2)			/* カーソル左シフト */
#define LCD_SHIFT_R_CUR	(1<<2)			/* カーソル右シフト */
#define LCD_SHIFT_L_DSP	(2<<2)			/* 表示全体左シフト */
#define LCD_SHIFT_R_DSP	(3<<2)			/* 表示全体右シフト */

#define LCD_SET			(1<<5)		/* ファンクションセット */
#define LCD_SET_8		(1<<4)			/* インタフェース長８ビット */
#define LCD_SET_4		(0<<4)			/* インタフェース長４ビット */
#define LCD_SET_2LINES	(1<<3)			/* ２行表示 */
#define LCD_SET_1LINE	(0<<3)			/* １行表示 */
#define LCD_SET_BIG		(1<<2)			/* フォント大 */
#define LCD_SET_SMALL	(0<<2)			/* フォント小 */

#define LCD_CGADDR		(1<<6)		/* CGRAMアドレスセット */

#define LCD_DDADDR		(1<<7)		/* DDRAMアドレスセット */
#define LINE_2_BASE		(0x40)		/* ２行目のベースアドレス */

#define LCD_BUSY		(1<<7)		/* ビジーフラグ／アドレス読み出し */

#endif	/* _LCD_H */

/*------------------------- end of lcd.h --------------------------*/
