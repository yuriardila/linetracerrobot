/**********************************************************************
 *
 *		システム名:	ANSIエスケープシーケンス用ヘッダ（MN101C49KのLCD用）
 *
 *		Copyright (c) 2003 松下電器産業（株）　ソフト・技術研修グループ
 *
 *---------------------------------------------------------------------
 *
 *		$RCSfile: esclcd.h,v $			$Revision: 1.3 $
 *
 *		$設　計: 森本泰司				$日付: 2003/ 7/26
 *		$Author: Y.Morimoto $			$Date: 2003-10-15 10:29:31+09 $
 *
 **********************************************************************/

#ifndef _ESCLCD_H
#define _ESCLCD_H

/* 関数プロトタイプ宣言 */
void lcd_locate(int x, int y);

/* マクロ定義 */
#define lcd_cls()			lcd_puts("\x1b[2J")
#define lcd_cursor_on()		lcd_puts("\x1b[>5l")
#define lcd_cursor_off()	lcd_puts("\x1b[>5h")

#endif		/* _ESCLCD_H */
/*----------------- end of esclcd.h -----------------------------------*/
