/**********************************************************************
 *
 *		システム名:	LCD制御（MN101C49K用）
 *
 *		Copyright (c) 2003 松下電器産業（株）　ソフト・技術研修グループ
 *
 *---------------------------------------------------------------------
 *
 *		$RCSfile: lcd.c,v $				$Revision: 1.3 $
 *
 *		$設　計: 森本泰司				$日付: 2003/ 7/26
 *		$Author: Y.Morimoto $			$Date: 2005-09-15 15:59:07+09 $
 *
 **********************************************************************/

#include <ctype.h>

#include "general.h"
#include "trainer.h"
#include "lcd.h"

static const char rcs_id[] = "$Id: lcd.c,v 1.3 2005-09-15 15:59:07+09 Y.Morimoto Exp $";

#define BUF_SIZE		(80)
#define TMOUT_LCD		(10)		/* 100ms：10msごとに10カウント */
#define ERR_TOUT		(1)
#define ERR_BFULL		(2)

static char buf_lcd[BUF_SIZE];		/* LCD用リングバッファ */
static char *write_ptr, *read_ptr;
static int cnt;						/* バッファに溜まっている文字の数 */
static int chk_tmout;				/* LCDの応答のタイムアウトチェック用 */
static int err_lcd;					/* LCD制御でのエラー番号 */

/* 関数プロトタイプ宣言 */
unsigned int get_time(void);

static int read_buf(void);
static unsigned int read_cmd(void);
static void write_cmd(int cmd);
static unsigned int read_data(void);
static void write_data(int data);
static bool_t is_ready(void);
void wait_100us(unsigned int time);	/* 初期化以外では使わないこと */

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *	関数名 :	init_lcd	LCD表示器の初期化
 *
 *	書　式 :	void init_lcd(void);
 *
 *	機　能 :	1.I/F長８ビット、２行表示、デューティ1/16を設定
 *			2.まず、表示をクリアして、表示モードオフ→オンにする
 *			3.エントリーモードをアドレスインクリメント、
 *			   表示シフトなしにする
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void init_lcd(void)
{
	/* 15ms以上 wait */
    wait_100us(150);
    
	/* 強制的に８ビットに */
	write_cmd(LCD_SET | LCD_SET_8);

	/* 4.1ms以上 */
    wait_100us(41);
    
	/* 強制的に８ビットに */
	write_cmd(LCD_SET | LCD_SET_8);

	/* 100μs以上 */
    wait_100us(1);
    
	/* 強制的に８ビットに、これで確立 */
	write_cmd(LCD_SET | LCD_SET_8);

	/* 100μs以上 */
    wait_100us(1);
    
	write_cmd(LCD_SET | LCD_SET_8 | LCD_SET_2LINES);
	while (is_ready() == No)
		;
	write_cmd(LCD_DSP | LCD_DSP_OFF);
	while (is_ready() == No)
		;
	write_cmd(LCD_CLR_DSP);
	while (is_ready() == No)
		;
	write_cmd(LCD_DSP | LCD_DSP_ON | LCD_DSP_CUR_ON | LCD_DSP_NOBLINK);
	while (is_ready() == No)
		;
	write_cmd(LCD_EMODE | LCD_EMODE_INC | LCD_EMODE_NOSFT);
	while (is_ready() == No)
		;

	reset_lcdbuf();
}
/*----------------- end of init_lcd() --------------------- EJECT ----*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *	関数名 :	ctrl_lcd	LCDへのデータ出力制御
 *
 *	書　式 :	int ctrl_lcd(void)
 *
 *	機　能 :	1.バッファが空であれば何もしない。
 *				2.LCDがビジーならば何もしない。
 *					ただし、一定時間以上なければエラーとする。
 *				3.以上でなければ、読み出しポインタの１文字を出力する。
 *				4.読み出しポインタを進め、最後まで来れば先頭に戻す。
 *
 *	結　果 :	戻り値：LCD関連のエラー番号
 *				ファイルスタティック変数 char *write_ptr, *read_ptr;
 *				ファイルスタティック変数 int cnt;
 *				ファイルスタティック変数 int err_lcd;	LCD関連のエラー
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

int ctrl_lcd(void)
{
	static unsigned int chk_10ms;

	if (cnt != 0) {						/* バッファにデータがある */
		if (is_ready() == Yes) {
			int c;

			c = read_buf();
			write_data(c);
			chk_tmout = TMOUT_LCD;		/* タイムオーバーチェック関連初期化 */
			chk_10ms = get_time_10ms();
		} else {
			if (get_time_10ms() != chk_10ms) {	/* 10msごと */
				++chk_10ms;
				--chk_tmout;
				if (chk_tmout <= 0) {		/* タイムオーバー */
					err_lcd = ERR_TOUT;
				}
			}
		}
	}
	return err_lcd;
}
/*------------------ end of ctrl_lcd() -------------------- EJECT ----*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *	関数名 :	lcd_puts	文字列出力
 *
 *	書　式 :	int lcd_puts(const char *s)
 *				char *s;	文字列の先頭番地
 *
 *	機　能 :	1.ナル文字を見つけるまでバッファに書く。
 *					最後に改行文字付加しない。
 *				2.エラーがあれば、ＥＯＦを返す。（なければ０）
 *
 *	結　果 :	関数値：０：正常終了、ＥＯＦ：異常終了
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

int lcd_puts(const char *s)
{
	while (*s != '\0') {
		lcd_putchar(*s);
		++s;
	}

	return err_lcd ? EOF : 0;
}
/*------------------ end of lcd_puts() -------------------------------*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *	関数名 :	lcd_puts	文字列出力
 *
 *	書　式 :	int lcd_puts(const char *s)
 *				char *s;	文字列の先頭番地
 *              unsigned char line;    行
 *              unsigned char cursor;  文字の位置
 *
 *	機　能 :	1.ナル文字を見つけるまでバッファに書く。
 *					最後に改行文字付加しない。
 *				2.エラーがあれば、ＥＯＦを返す。（なければ０）
 *
 *	結　果 :	関数値：０：正常終了、ＥＯＦ：異常終了
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

int lcd_puts_line_loc(const char *s, unsigned char line, unsigned char cursor)
{
    static unsigned int chk_10ms;
    
	while (is_ready() == No)
		;
	write_cmd(0x80|((line&0x01)<<6)|(cursor&0x0f));
    
    while (*s != '\0') {
        if (is_ready() == Yes) {
            write_data(*s++);
            chk_tmout = TMOUT_LCD;		/* タイムオーバーチェック関連初期化 */
            chk_10ms = get_time_10ms();
        } else {
            if (get_time_10ms() != chk_10ms) {	/* 10msごと */
                ++chk_10ms;
                --chk_tmout;
                if (chk_tmout <= 0) {		/* タイムオーバー */
                    err_lcd = ERR_TOUT;
                }
            }
        }
	}

	return err_lcd ? EOF : 0;
}
/*------------------ end of lcd_puts() -------------------------------*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *	関数名 :	lcd_putchar		LCDバッファへの１文字出力
 *
 *	書　式 :	int lcd_putchar(int c)
 *				int c;		出力データ
 *
 *	機　能 :	1.バッファが一杯であれば、エラーを返す。
 *				2.なければ、ライトポインタの場所に１文字書き、ポインタ
 *					を進め、最後まで来れば、先頭に戻す。
 *				3.バッファのカウンタをインクリメントする
 *
 *	結　果 :	関数値：０：正常終了、ＥＯＦ：異常終了
 *				ファイルスタティック変数 char *write_ptr, *read_ptr;
 *				ファイルスタティック変数 int cnt;
 *				ファイルスタティック変数 int err_lcd;
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

int lcd_putchar(int c)
{
    if (cnt >= BUF_SIZE) {
        err_lcd = ERR_BFULL;
    } else {
        *write_ptr = c;
        write_ptr++;
        if(write_ptr == buf_lcd + BUF_SIZE) write_ptr = buf_lcd;
        cnt++;
    }

    return err_lcd ? EOF : 0;
}
/*------------------ end of lcd_putchar() ----------------- EJECT ----*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *	関数名 :	read_buf		LCDバッファから１文字入力
 *
 *	書　式 :	int read_buf(void)
 *
 *	機　能 :	1.バッファが空であれば、エラーを返す。
 *				2.あれば、リードポインタの場所から１文字読み、ポインタ
 *					を進め、最後まで来れば、先頭に戻す。
 *				3.バッファのカウンタをデクリメントする
 *
 *	結　果 :	関数値：非負：正常終了、ＥＯＦ：異常終了
 *				ファイルスタティック変数 char *write_ptr, *read_ptr;
 *				ファイルスタティック変数 int cnt;
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

static int read_buf(void)
{
    int c;
    if (cnt == 0) {
        return EOF;
    } else {
        c = *read_ptr;
        read_ptr++;
        if (read_ptr == (buf_lcd + BUF_SIZE)) read_ptr = buf_lcd;
        cnt--;
    }    
    
	return c;
}
/*------------------ end of read_buf() -------------------- EJECT ----*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *	関数名 :	reset_lcdbuf	LCD制御関連のリセット
 *
 *	書　式 :	void reset_lcdbuf(void)
 *
 *	機　能 :	1.LCD用バッファのポインタ、フラグを初期化する。
 *
 *	結　果 :
 *				ファイルスタティック変数 char *write_ptr, *read_ptr;
 *				ファイルスタティック変数 int cnt;
 *				ファイルスタティック変数 int err_lcd;
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

void reset_lcdbuf(void)
{
	write_ptr = read_ptr = buf_lcd;
	cnt = 0;
	chk_tmout = TMOUT_LCD;
	err_lcd = 0;
}
/*----------------- end of reset_lcdbuf() ----------------- EJECT ----*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *	関数名 :	is_ready		LCDビジーチェック
 *
 *	書　式 :	Bool is_ready(void)
 *
 *	機　能 :	1.LCDのビジーフラグをチェックする
 *
 *	結　果 :	関数値：Yes/No
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

static bool_t is_ready(void)
{
	return (read_cmd() & LCD_BUSY) ? No : Yes;
}
/*----------------- end of is_ready() --------------------- EJECT ----*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *	関数名 :	read_cmd	コマンドレジスタからの読み出し
 *
 *	書　式 :	unsigned int read_cmd(void)
 *
 *	機　能 :	1.コマンドレジスタの内容を読み出す
 *
 *	注　意 :	LCDとのデータのやりとりは双方向バスを使っているので、
 *				CPUのポートとLCDが同時に出力にならないようにすること。
 *					write_cmd、read_data、write_dataも同様
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

static unsigned int read_cmd(void)
{
	unsigned int buf;

	outp(LCD_DB_PLU, SET_PULLUP);		/* まず、プルアップしてから */
	outp(LCD_DB_DIR, SET_INPUT);		/* 双方向バスを入力に（オープンにしない） */
	outp(LCD_RS_SEL, LCD_CMD_SEL);
	outp(LCD_RW_CTRL, LCD_READ);		/* 次に、LCDを出力（READ）に */
	outp(LCD_RW_CTRL, LCD_READ | LCD_EN);
	buf = inp(LCD_DATA_R);
	outp(LCD_RW_CTRL, LCD_READ);

	return buf;
}
/*----------------- end of read_cmd() -------------------- EJECT ----*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *	関数名 :	write_cmd	コマンドレジスタへの書き込み
 *
 *	書　式 :	void write_cmd(int cmd)
 *
 *	機　能 :	1.与えられたデータをコマンドレジスタに書き込む
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

static void write_cmd(int cmd)
{
	outp(LCD_RW_CTRL, LCD_WRITE);		/* まず、LCDを入力（WRITE）に */
	outp(LCD_DB_DIR, SET_OUTPUT);		/* 次に、双方向バスを出力にしてから */
	outp(LCD_DB_PLU, RST_PULLUP);		/* プルアップをはずす（オープンにしない） */
	outp(LCD_DATA_W, cmd);
	outp(LCD_RS_SEL, LCD_CMD_SEL);
	outp(LCD_RW_CTRL, LCD_WRITE | LCD_EN);
	outp(LCD_RW_CTRL, LCD_WRITE);			/* LCD_E OFF */
}
/*----------------- end of write_cmd() -------------------- EJECT ----*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *	関数名 :	read_data	データレジスタからの読み出し
 *
 *	書　式 :	unsigned int read_data(void)
 *
 *	機　能 :	1.データレジスタの内容を読み出す
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

static unsigned int read_data(void)
{
	unsigned int buf;

	outp(LCD_DB_PLU, SET_PULLUP);		/* まず、プルアップしてから */
	outp(LCD_DB_DIR, SET_INPUT);		/* 双方向バスを入力に（オープンにしない） */
	outp(LCD_RS_SEL, LCD_DATA_SEL);
	outp(LCD_RW_CTRL, LCD_READ);		/* 次に、LCDを出力（READ）に */
	outp(LCD_RW_CTRL, LCD_READ | LCD_EN);
	buf = inp(LCD_DATA_R);
	outp(LCD_RW_CTRL, LCD_READ);
	outp(LCD_RS_SEL, LCD_CMD_SEL);

	return buf;
}
/*----------------- end of read_data() ------------------- EJECT ----*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *	関数名 :	write_data	データレジスタへの書き込み
 *
 *	書　式 :	void write_data(int data)
 *
 *	機　能 :	1.与えられたデータをデータレジスタに書き込む
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

static void write_data(int data)
{
	outp(LCD_RW_CTRL, LCD_WRITE);		/* まず、LCDを入力（WRITE）に */
	outp(LCD_DB_DIR, SET_OUTPUT);		/* 次に、双方向バスを出力にしてから */
	outp(LCD_DB_PLU, RST_PULLUP);		/* プルアップをはずす（オープンにしない） */
	outp(LCD_DATA_W, data);
	outp(LCD_RS_SEL, LCD_DATA_SEL);
	outp(LCD_RW_CTRL, LCD_WRITE | LCD_EN);
	outp(LCD_RW_CTRL, LCD_WRITE);
	outp(LCD_RS_SEL, LCD_CMD_SEL);
}
/*----------------- end of write_data() ------------------- EJECT ----*/
/*----------------- end of lcd.c -------------------------------------*/
