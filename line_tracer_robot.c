/*******************************************************************************
 *
 *      システム名：　ラインとレーサーロボット
 *
 *		Copyright (c) Yuri Ardila 2015
 *
 *-----------------------------------------------------------------------------
 *
 *		$RCSfile: line_tracer_robot.c,v $		$Revision: 1.0 $
 *
 *		$設　計: Yuri Ardila			$日付: 2015/06/06
 *		$Author: Yuri Ardila $		$Date: 2015-06-06 17:08:02+09 $
 *
 *******************************************************************************/

#include "line_tracer_robot.h"

static st_line_tracer_state_t g_st_line_tracer_states[LTR_MAX_STATES];  /* 状態リングバッファ */
static uint8_t g_line_tracer_state_id;                                  /* 状態リングバッファのインデックス */
static uint8_t g_line_tracer_state_err;                                 /* エラーコード */
static unsigned int g_line_tracer_state_ctr;                            /* 状態カウンタ　*/

static uint8_t g_line_tracer_state_min_speed_period;                    /* ロボットの最低速度 */
static uint8_t g_line_tracer_state_max_speed_period;                    /* ロボットの最高速度 */

/***********************************************************************

                            Robot interface 

************************************************************************/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_init_robot      ライントレースロボットの初期化
 *
 *  書　式 :   void f_init_robot(void)
 *
 *  機　能 :   1.ライントレースロボットの状態リングバッファの初期化
 *            2.タイマーの初期化
 *            3.最初のトグルスイッチの値を読み込み、ライントレースロボットの最高速度をセット
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void f_init_robot(void) {
    
    int i;

    uint8_t cur_tsw;

    reset_timers();

    g_line_tracer_state_id = 0;
    g_line_tracer_state_ctr = 0;
    g_line_tracer_state_err = 0;

    for (i = 0; i < LTR_MAX_STATES; i++) {
        f_init_state(&g_st_line_tracer_states[i]);
    }

    /* ライントレースロボットの最高速度をトグルスイッチから設定する */
    wait_fix_tsw();
    cur_tsw = get_tsw();
    g_line_tracer_state_min_speed_period = LTR_MIN_SPEED_PERIOD;
    if (cur_tsw >= 1 && cur_tsw <= g_line_tracer_state_min_speed_period) {
        g_line_tracer_state_max_speed_period = cur_tsw;
    } else {
        g_line_tracer_state_max_speed_period = LTR_MAX_SPEED_PERIOD;
    }

    return;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_line_tracer_robot      ライントレースロボットのメインインタフェース
 *
 *  書　式 :   void f_line_tracer_robot(void)
 *
 *  機　能 :   1. リングバッファを利用しながら、ロボットの状態を更新してラインをトレースする
 *            2. 1ms毎にロボットの周期をチェック。0になったら、モータを回す
 *            3. f_predict_next_state()関数で現状態と前状態を比較し、次の状態を計算する
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void f_line_tracer_robot(void) {

    uint8_t buf_rwh = 0x99;
    uint8_t buf_lwh = 0x99;

    uint8_t buf_lwh_ctr;
    uint8_t buf_rwh_ctr;

    uint8_t cur_sns;

    unsigned int chk_1ms;
    unsigned int buf_chk_1ms;

    /* Get previous, current, and next state */
    st_line_tracer_state_t *l_line_tracer_cur_state = f_get_current_state();
    st_line_tracer_state_t *l_line_tracer_nxt_state = f_get_next_state();
    st_line_tracer_state_t *l_line_tracer_prv_state = f_get_previous_state();

    buf_lwh_ctr = l_line_tracer_cur_state->rbt_speed_timer_l_wheel;
    buf_rwh_ctr = l_line_tracer_cur_state->rbt_speed_timer_r_wheel;

    FOREVER {

        /* Wait until sensor fixed to middle */
        /* (is_fix_middle is set to 1) */
        wait_fix_mid();

        /* Process wheel movement based on current speed */
        chk_1ms = get_time_1ms();
        if (buf_chk_1ms != chk_1ms) {
            buf_chk_1ms = chk_1ms;
            buf_lwh_ctr--;
            buf_rwh_ctr--;
            if (l_line_tracer_cur_state->rbt_movement_state != RBT_MOV_STP) l_line_tracer_cur_state->rbt_period_ms_ctr++;
        }

        if (buf_lwh_ctr <= 0) {
            
            if (l_line_tracer_cur_state->rbt_speed_timer_l_wheel != 0) {
                buf_lwh = rotate_r(buf_lwh);
                outp(LWH, buf_lwh);
            }

            buf_lwh_ctr = l_line_tracer_cur_state->rbt_speed_timer_l_wheel;
        }

        if (buf_rwh_ctr <= 0) {
            
            if (l_line_tracer_cur_state->rbt_speed_timer_r_wheel != 0) {
                buf_rwh = rotate_l(buf_rwh);
                outp(RWH, buf_rwh);
            }

            buf_rwh_ctr = l_line_tracer_cur_state->rbt_speed_timer_r_wheel;
        }

        /* Process errors */

        /* Predict next state and change state if the condition is met */
        cur_sns = get_sns();
        if (f_predict_next_state(l_line_tracer_cur_state, 
                                 l_line_tracer_nxt_state, 
                                 l_line_tracer_prv_state,
                                 cur_sns)) 
        {
            f_change_state(&l_line_tracer_cur_state, 
                           &l_line_tracer_nxt_state, 
                           &l_line_tracer_prv_state);
        }
    }

    f_done_state(l_line_tracer_cur_state);

    return;
}

/***********************************************************************

                    State Related (Global) Functions

************************************************************************/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_init_state      状態の初期化
 *
 *  書　式 :   void f_init_state(void)
 *
 *  機　能 :   ロボット状態の初期化
 *            1. rbt_movement_state         : ロボットの動作状態. 初期状態: RBT_MOV_STP
 *            2. rbt_sensor_state           : ロボットのセンサー状態. 初期状態: RBT_SNS_UNKNOWN 
 *            3. rbt_speed_timer_l_wheel    : ロボットの左タイヤの周期. 初期状態: 0
 *            4. rbt_speed_timer_r_wheel    : ロボットの右タイヤの周期. 初期状態: 0
 *            5. rbt_period_ms_ctr          : 現状態の1ms経過カウンタ. 初期状態: 0
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void f_init_state(st_line_tracer_state_t *state) {
    state->rbt_movement_state = RBT_MOV_STP;
    state->rbt_sensor_state = RBT_SNS_UNKNOWN;
    state->rbt_speed_timer_l_wheel = 0;
    state->rbt_speed_timer_r_wheel = 0;
    state->rbt_period_ms_ctr = 0;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_predict_next_state      次の状態を予測（計算）
 *
 *  書　式 :   void f_predict_next_state(void)
 *
 *  機　能 :   読み取ったセンサー入力を現状態のセンサー状態と比較し、状態を変更するかのチェックを行う
 *
 *　結果   :  True: 現状態を次の状態に更新  False: 現状態を維持
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
bool_t f_predict_next_state(st_line_tracer_state_t *l_line_tracer_cur_state,
                            st_line_tracer_state_t *l_line_tracer_nxt_state,
                            st_line_tracer_state_t *l_line_tracer_prv_state,
                            uint8_t l_cur_sns) 
{    
    /* Read sensor first */
    uint8_t cur_sns = l_cur_sns;
    uint8_t buf_sns = l_line_tracer_cur_state->rbt_sensor_state;

    /* return val */
    bool_t ret = False;

    /* Analyze next state based on current state and sensor */
    if (cur_sns != buf_sns) {
        
        l_line_tracer_nxt_state->rbt_sensor_state = cur_sns;

        /* Change to next state on return */
        ret = True;

        /* If state is changed, then we are sure it's not offline */
        set_fix_off_0();

        /* Change in sensor. Prepare next state. */
        switch (cur_sns) {
            case RBT_SNS_MIDDLE:    
                f_run_straight(l_line_tracer_nxt_state, l_line_tracer_cur_state);
            break;

            case RBT_SNS_S_LEFT:
                f_turn_right_5deg(l_line_tracer_nxt_state, l_line_tracer_cur_state);
            break;

            case RBT_SNS_M_LEFT:
                f_turn_right_10deg(l_line_tracer_nxt_state, l_line_tracer_cur_state);
            break;

            case RBT_SNS_L_LEFT:
                f_turn_right_20deg(l_line_tracer_nxt_state, l_line_tracer_cur_state);
            break;

            case RBT_SNS_S_RIGHT:
                f_turn_left_5deg(l_line_tracer_nxt_state, l_line_tracer_cur_state);
            break;

            case RBT_SNS_M_RIGHT:
                f_turn_left_10deg(l_line_tracer_nxt_state, l_line_tracer_cur_state);
            break;

            case RBT_SNS_L_RIGHT:
                f_turn_left_20deg(l_line_tracer_nxt_state, l_line_tracer_cur_state);
            break;

            case RBT_SNS_OFFLINE:
                f_soft_shutdown(l_line_tracer_nxt_state, l_line_tracer_cur_state);
            break;

            case RBT_SNS_CROSSLINE:
                /* Ignore changes for crossline. Keep current state. */
                f_do_nothing();
                ret = False;
            break;

            case RBT_SNS_UNKNOWN:
                f_hard_shutdown(l_line_tracer_nxt_state, l_line_tracer_cur_state);
            break;
            
            default:
                /* Ignore unknown sensor value */
                f_do_nothing();
                ret = False;
            break;
            
        }

    } else {

        /* Sensor is not changed. See if we can accelerate speed. */
        switch (cur_sns) {
            case RBT_SNS_MIDDLE:    
            case RBT_SNS_S_LEFT:
            /*case RBT_SNS_M_LEFT:
            case RBT_SNS_L_LEFT:*/
            case RBT_SNS_S_RIGHT:
            /*case RBT_SNS_M_RIGHT:
            case RBT_SNS_L_RIGHT:*/
            case RBT_SNS_CROSSLINE:

                /* Check if period ctr fulfills the condition for speedup */
                if (l_line_tracer_cur_state->rbt_period_ms_ctr >= LTR_ACCEL_TIME && l_line_tracer_cur_state->rbt_movement_state != RBT_MOV_STP) {

                    /* Check if speed is not the fastest */
                    if (l_line_tracer_cur_state->rbt_speed_timer_l_wheel > g_line_tracer_state_max_speed_period && l_line_tracer_cur_state->rbt_speed_timer_r_wheel > g_line_tracer_state_max_speed_period) {
                        f_run_accelerate(l_line_tracer_nxt_state, l_line_tracer_cur_state);
                        ret = True;
                    }
                    /* There is a condition where prev state and cur state is different, but the speed is still 0 */
                    /* For this case, we initiate both wheels with minimum speed period */
                    else if (!l_line_tracer_cur_state->rbt_speed_timer_l_wheel && !l_line_tracer_cur_state->rbt_speed_timer_r_wheel) {
                        /* Initiate with min speed */
                        l_line_tracer_nxt_state->rbt_speed_timer_r_wheel = g_line_tracer_state_min_speed_period;
                        l_line_tracer_nxt_state->rbt_speed_timer_l_wheel = g_line_tracer_state_min_speed_period;
                        l_line_tracer_nxt_state->rbt_movement_state = RBT_MOV_RUN_STRAIGHT;
                        ret = True;
                    }

                }

            break;

            case RBT_SNS_OFFLINE:
                /* Check if period ctr fulfills the condition for speeddown */
                if (l_line_tracer_cur_state->rbt_period_ms_ctr >= LTR_DECEL_TIME && l_line_tracer_cur_state->rbt_movement_state != RBT_MOV_STP) {

                    /* Check if robot is not stopping */
                    if (l_line_tracer_nxt_state->rbt_speed_timer_r_wheel < g_line_tracer_state_min_speed_period && l_line_tracer_nxt_state->rbt_speed_timer_l_wheel < g_line_tracer_state_min_speed_period) {
                        f_run_decelerate(l_line_tracer_nxt_state, l_line_tracer_cur_state);
                    } else {
                        l_line_tracer_nxt_state->rbt_speed_timer_r_wheel = g_line_tracer_state_min_speed_period;
                        l_line_tracer_nxt_state->rbt_speed_timer_l_wheel = g_line_tracer_state_min_speed_period;
                        l_line_tracer_nxt_state->rbt_movement_state = l_line_tracer_cur_state->rbt_movement_state;
                    }

                    /* If the time exceeds threshold of offline, then we stop the movement */
                    if (get_fix_off() == BIT_PATTERN_0) {
                        l_line_tracer_nxt_state->rbt_speed_timer_r_wheel = 0;
                        l_line_tracer_nxt_state->rbt_speed_timer_l_wheel = 0;
                        l_line_tracer_nxt_state->rbt_movement_state = RBT_MOV_STP;
                        set_fix_mid_0();
                    }

                    ret = True; 
                }
            break;

            default:
                /* Keep current state */
                f_do_nothing();
                ret = False;
            break;
        }
    }

    return ret;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_change_state      状態の変更
 *
 *  書　式 :   void f_change_state(void)
 *
 *  機　能 :   現状態を次のインデックス(g_line_tracer_state_id + 1)で指定する状態に変更
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void f_change_state(st_line_tracer_state_t **l_line_tracer_cur_state,
                    st_line_tracer_state_t **l_line_tracer_nxt_state,
                    st_line_tracer_state_t **l_line_tracer_prv_state) {
    g_line_tracer_state_id = (g_line_tracer_state_id + 1) % LTR_MAX_STATES;
    *l_line_tracer_prv_state = *l_line_tracer_cur_state;
    *l_line_tracer_cur_state = *l_line_tracer_nxt_state;
    *l_line_tracer_nxt_state = f_get_next_state();
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_done_state      終了状態に変更
 *
 *  書　式 :   void f_done_state(void)
 *
 *  機　能 :   終了状態に変更（停止状態にする）
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
void f_done_state(st_line_tracer_state_t* state) {
    state->rbt_movement_state = RBT_MOV_STP;
    state->rbt_speed_timer_l_wheel = 0;
    state->rbt_speed_timer_r_wheel = 0;
    state->rbt_period_ms_ctr = 0;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_get_current_state      現状態を取得
 *
 *  書　式 :   void f_get_current_state(void)
 *
 *  機　能 :   終了状態に変更（停止状態にする）
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
st_line_tracer_state_t* f_get_current_state() {
    st_line_tracer_state_t* ret;
    ret = &g_st_line_tracer_states[g_line_tracer_state_id];
    return ret;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_get_previous_state      1つ前の状態を取得
 *
 *  書　式 :   void f_get_previous_state(void)
 *
 *  機　能 :   終了状態に変更（停止状態にする）
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
st_line_tracer_state_t* f_get_previous_state() {
    st_line_tracer_state_t* ret;
    uint8_t prev_id = (g_line_tracer_state_id != 0) ? (g_line_tracer_state_id--) : (LTR_MAX_STATES-1);
    ret = &g_st_line_tracer_states[prev_id];
    return ret;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_get_next_state      1つ前の状態を取得
 *
 *  書　式 :   void f_get_next_state(void)
 *
 *  機　能 :   終了状態に変更（停止状態にする）
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
st_line_tracer_state_t* f_get_next_state() {
    st_line_tracer_state_t* ret;
    uint8_t next_id = (g_line_tracer_state_id != LTR_MAX_STATES-1) ? (g_line_tracer_state_id++) : (0);
    ret = &g_st_line_tracer_states[next_id];
    return ret;
}

/***********************************************************************

                    Robot Static Functions 

************************************************************************/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_run_accelerate      加速制御
 *
 *  書　式 :   void f_run_accelerate(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state)
 *              st_line_tracer_state_t* nxt_state           次の状態のメモリ番地
 *              const st_line_tracer_state_t* cur_state     現在の状態のメモリ番地
 *
 *  機　能 :   加速制御(速度の周期を1ms早くする). 動作状態（まっすぐ走るか曲がるか）は現状態維持
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
static void f_run_accelerate(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state) {
    nxt_state->rbt_sensor_state = cur_state->rbt_sensor_state;
    nxt_state->rbt_movement_state = cur_state->rbt_movement_state;

    /* Do acceleration of 1ms for each wheel */
    nxt_state->rbt_speed_timer_r_wheel = cur_state->rbt_speed_timer_r_wheel - 1;
    nxt_state->rbt_speed_timer_l_wheel = cur_state->rbt_speed_timer_l_wheel - 1;

    nxt_state->rbt_period_ms_ctr = 0;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_run_decelerate      減速制御
 *
 *  書　式 :   void f_run_decelerate(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state)
 *              st_line_tracer_state_t* nxt_state           次の状態のメモリ番地
 *              const st_line_tracer_state_t* cur_state     現在の状態のメモリ番地
 *
 *  機　能 :   減速制御(速度の周期を1ms遅くする). 動作状態（まっすぐ走るか曲がるか）は現状態維持
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
static void f_run_decelerate(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state) {
    nxt_state->rbt_sensor_state = cur_state->rbt_sensor_state;
    nxt_state->rbt_movement_state = cur_state->rbt_movement_state;
    
    /* Do deceleration of 1ms for each wheel */
    nxt_state->rbt_speed_timer_r_wheel = cur_state->rbt_speed_timer_r_wheel + 1;
    nxt_state->rbt_speed_timer_l_wheel = cur_state->rbt_speed_timer_l_wheel + 1;

    nxt_state->rbt_period_ms_ctr = 0;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_run_straight      まっすぐ走る（センサー状態がRBT_SNS_MIDDLEの状態である）
 *
 *  書　式 :   void f_run_straight(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state)
 *              st_line_tracer_state_t* nxt_state           次の状態のメモリ番地
 *              const st_line_tracer_state_t* cur_state     現在の状態のメモリ番地
 *
 *  機　能 :   1. 停止状態からだと、両タイヤを最低速度で走らせる
 *            2. 走っている状態からだと、速い方を持つタイヤの速度で走る
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
static void f_run_straight(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state) {
    
    unsigned char tmp_spd;

    /* 
        If changing to RBT_SNS_MIDDLE after turning, use the fastest speed of both wheels.
        If changing from RBT_MOV_STP, initiate with 4ms.
    */
    nxt_state->rbt_movement_state = RBT_MOV_RUN_STRAIGHT;
    if (cur_state->rbt_movement_state == RBT_MOV_STP) {
        nxt_state->rbt_speed_timer_l_wheel = g_line_tracer_state_min_speed_period;
        nxt_state->rbt_speed_timer_r_wheel = g_line_tracer_state_min_speed_period;
    } 
    /* Use the smallest period */
    else {
        tmp_spd = (cur_state->rbt_speed_timer_l_wheel < cur_state->rbt_speed_timer_r_wheel && cur_state->rbt_speed_timer_l_wheel) ?
                cur_state->rbt_speed_timer_l_wheel : cur_state->rbt_speed_timer_r_wheel;
        
        nxt_state->rbt_speed_timer_l_wheel = tmp_spd;
        nxt_state->rbt_speed_timer_r_wheel = tmp_spd;
    }

    nxt_state->rbt_period_ms_ctr = 0;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_turn_left_5deg      （小）左折する（センサー状態がRBT_SNS_S_LEFTの状態である）
 *
 *  書　式 :   void f_turn_left_5deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state)
 *              st_line_tracer_state_t* nxt_state           次の状態のメモリ番地
 *              const st_line_tracer_state_t* cur_state     現在の状態のメモリ番地
 *
 *  機　能 :   1. 停止状態からだと、左タイヤを最低速度の周期+1、右タイヤを最低速度で走らせる
 *            2. 走っている状態からだと、速い方を持つタイヤの速度で走る
 *            3. ロボットがとっても速い速度で走ると（周期2ms）、左折せずにそのまままっすぐ走る
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
static void f_turn_left_5deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state) {

    unsigned char tmp_spd;

    /* 
        If changing to RBT_SNS_S_LEFT when moving, maintain the speed while turning.
        If changing from RBT_MOV_STP, initiate with 4ms and 5ms.
    */
    nxt_state->rbt_movement_state = RBT_MOV_TRN_LEFT_5DEG;
    if (cur_state->rbt_movement_state == RBT_MOV_STP) {
        nxt_state->rbt_speed_timer_l_wheel = g_line_tracer_state_min_speed_period + 1;
        nxt_state->rbt_speed_timer_r_wheel = g_line_tracer_state_min_speed_period;
    } 
    /* Use the smallest period */
    else {
        tmp_spd = (cur_state->rbt_speed_timer_l_wheel < cur_state->rbt_speed_timer_r_wheel && cur_state->rbt_speed_timer_l_wheel) ?
                cur_state->rbt_speed_timer_l_wheel : cur_state->rbt_speed_timer_r_wheel;
        if (tmp_spd <= 2) {
            nxt_state->rbt_speed_timer_l_wheel = tmp_spd;
            nxt_state->rbt_speed_timer_r_wheel = tmp_spd;
        } else {
            nxt_state->rbt_speed_timer_l_wheel = tmp_spd + 1;
            nxt_state->rbt_speed_timer_r_wheel = tmp_spd;
        }     
    }

    nxt_state->rbt_period_ms_ctr = 0;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_turn_left_10deg      （中）左折する（センサー状態がRBT_SNS_M_LEFTの状態である）
 *
 *  書　式 :   void f_turn_left_10deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state)
 *              st_line_tracer_state_t* nxt_state           次の状態のメモリ番地
 *              const st_line_tracer_state_t* cur_state     現在の状態のメモリ番地
 *
 *  機　能 :   1. 停止状態からだと、左タイヤを最低速度の周期+2、右タイヤを最低速度で走らせる
 *            2. 走っている状態からだと、左タイヤを速い方を持つタイヤの速度、右タイヤをその速度の周期+2で走る
 *            3. ロボットがとっても速い速度で走ると（周期2ms）、（小）左折を行う
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
static void f_turn_left_10deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state) {

    unsigned char tmp_spd;

    /* 
        If changing to RBT_SNS_M_LEFT when moving, maintain the speed while turning.
        If changing from RBT_MOV_STP, initiate with 3ms and 5ms.
    */
    nxt_state->rbt_movement_state = RBT_MOV_TRN_LEFT_10DEG;
    if (cur_state->rbt_movement_state == RBT_MOV_STP) {
        nxt_state->rbt_speed_timer_l_wheel = g_line_tracer_state_min_speed_period + 2;
        nxt_state->rbt_speed_timer_r_wheel = g_line_tracer_state_min_speed_period;
    } 
    /* Use the smallest period */
    else {
        tmp_spd = (cur_state->rbt_speed_timer_l_wheel < cur_state->rbt_speed_timer_r_wheel && cur_state->rbt_speed_timer_l_wheel) ?
                cur_state->rbt_speed_timer_l_wheel : cur_state->rbt_speed_timer_r_wheel;
        if (tmp_spd <= 2) {
            nxt_state->rbt_speed_timer_l_wheel = tmp_spd + 1;
            nxt_state->rbt_speed_timer_r_wheel = tmp_spd;
        } else {
            nxt_state->rbt_speed_timer_l_wheel = tmp_spd + 2;
            nxt_state->rbt_speed_timer_r_wheel = tmp_spd;
        }
    }

    nxt_state->rbt_period_ms_ctr = 0;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_turn_left_20deg      （大）左折する（センサー状態がRBT_SNS_M_LEFTの状態である）
 *
 *  書　式 :   void f_turn_left_20deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state)
 *              st_line_tracer_state_t* nxt_state           次の状態のメモリ番地
 *              const st_line_tracer_state_t* cur_state     現在の状態のメモリ番地
 *
 *  機　能 :   1. 停止状態からだと、左タイヤを最低速度の周期+2、右タイヤを最低速度の周期-1で走らせる
 *            2. 走っている状態からだと、左タイヤを速い方を持つタイヤの速度、右タイヤをその速度の周期+3で走る
 *            3. ロボットがとっても速い速度で走ると（周期2ms）、（中）左折を行う
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
static void f_turn_left_20deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state) {

    unsigned char tmp_spd;

    /* 
        If changing to RBT_SNS_L_LEFT when moving, maintain the speed while turning.
        If changing from RBT_MOV_STP, initiate with 2ms and 5ms.
    */
    nxt_state->rbt_movement_state = RBT_MOV_TRN_LEFT_20DEG;
    if (cur_state->rbt_movement_state == RBT_MOV_STP) {
        nxt_state->rbt_speed_timer_l_wheel = g_line_tracer_state_min_speed_period + 2;
        nxt_state->rbt_speed_timer_r_wheel = g_line_tracer_state_min_speed_period - 1;
    } 
    /* Use the smallest period */
    else {
        tmp_spd = (cur_state->rbt_speed_timer_l_wheel < cur_state->rbt_speed_timer_r_wheel && cur_state->rbt_speed_timer_l_wheel) ?
                cur_state->rbt_speed_timer_l_wheel : cur_state->rbt_speed_timer_r_wheel;

        if (tmp_spd <= 2) {
            nxt_state->rbt_speed_timer_l_wheel = tmp_spd + 2;
            nxt_state->rbt_speed_timer_r_wheel = tmp_spd;
        } else if (tmp_spd - 1 >= g_line_tracer_state_max_speed_period) {
            nxt_state->rbt_speed_timer_l_wheel = tmp_spd + 2;
            nxt_state->rbt_speed_timer_r_wheel = tmp_spd - 1;
        } else {
            nxt_state->rbt_speed_timer_l_wheel = tmp_spd + 3;
            nxt_state->rbt_speed_timer_r_wheel = tmp_spd;
        }
    }

    nxt_state->rbt_period_ms_ctr = 0;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_turn_right_5deg      （小）右折する（センサー状態がRBT_SNS_S_RIGHTの状態である）
 *
 *  書　式 :   void f_turn_right_5deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state)
 *              st_line_tracer_state_t* nxt_state           次の状態のメモリ番地
 *              const st_line_tracer_state_t* cur_state     現在の状態のメモリ番地
 *
 *  機　能 :   1. 停止状態からだと、右タイヤを最低速度の周期+1、左タイヤを最低速度で走らせる
 *            2. 走っている状態からだと、速い方を持つタイヤの速度で走る
 *            3. ロボットがとっても速い速度で走ると（周期2ms）、右折せずにそのまままっすぐ走る
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
static void f_turn_right_5deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state) {

    unsigned char tmp_spd;

    /* 
        If changing to RBT_SNS_S_RIGHT when moving, maintain the speed while turning.
        If changing from RBT_MOV_STP, initiate with 4ms and 5ms.
    */
    nxt_state->rbt_movement_state = RBT_MOV_TRN_RIGHT_5DEG;
    if (cur_state->rbt_movement_state == RBT_MOV_STP) {
        nxt_state->rbt_speed_timer_l_wheel = g_line_tracer_state_min_speed_period;
        nxt_state->rbt_speed_timer_r_wheel = g_line_tracer_state_min_speed_period + 1;
    } 
    /* Use the smallest period */
    else {
        tmp_spd = (cur_state->rbt_speed_timer_l_wheel < cur_state->rbt_speed_timer_r_wheel && cur_state->rbt_speed_timer_l_wheel) ?
                cur_state->rbt_speed_timer_l_wheel : cur_state->rbt_speed_timer_r_wheel;
        if (tmp_spd <= 2) {
            nxt_state->rbt_speed_timer_l_wheel = tmp_spd;
            nxt_state->rbt_speed_timer_r_wheel = tmp_spd;
        } else {
            nxt_state->rbt_speed_timer_l_wheel = tmp_spd;
            nxt_state->rbt_speed_timer_r_wheel = tmp_spd + 1;
        }   
    }

    nxt_state->rbt_period_ms_ctr = 0;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_turn_right_10deg      （中）右折する（センサー状態がRBT_SNS_M_RIGHTの状態である）
 *
 *  書　式 :   void f_turn_right_10deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state)
 *              st_line_tracer_state_t* nxt_state           次の状態のメモリ番地
 *              const st_line_tracer_state_t* cur_state     現在の状態のメモリ番地
 *
 *  機　能 :   1. 停止状態からだと、右タイヤを最低速度の周期+2、左タイヤを最低速度で走らせる
 *            2. 走っている状態からだと、右タイヤを速い方を持つタイヤの速度、左タイヤをその速度の周期+2で走る
 *            3. ロボットがとっても速い速度で走ると（周期2ms）、（小）右折を行う
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
static void f_turn_right_10deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state) {

    unsigned char tmp_spd;

    /* 
        If changing to RBT_SNS_S_RIGHT when moving, maintain the speed while turning.
        If changing from RBT_MOV_STP, initiate with 3ms and 5ms.
    */
    nxt_state->rbt_movement_state = RBT_MOV_TRN_RIGHT_10DEG;
    if (cur_state->rbt_movement_state == RBT_MOV_STP) {
        nxt_state->rbt_speed_timer_l_wheel = g_line_tracer_state_min_speed_period;
        nxt_state->rbt_speed_timer_r_wheel = g_line_tracer_state_min_speed_period + 2;
    } 
    /* Use the smallest period */
    else {
        tmp_spd = (cur_state->rbt_speed_timer_l_wheel < cur_state->rbt_speed_timer_r_wheel && cur_state->rbt_speed_timer_l_wheel) ?
                cur_state->rbt_speed_timer_l_wheel : cur_state->rbt_speed_timer_r_wheel;
        if (tmp_spd <= 2) {
            nxt_state->rbt_speed_timer_l_wheel = tmp_spd;
            nxt_state->rbt_speed_timer_r_wheel = tmp_spd + 1;
        } else {
            nxt_state->rbt_speed_timer_l_wheel = tmp_spd;
            nxt_state->rbt_speed_timer_r_wheel = tmp_spd + 2;
        }
    }

    nxt_state->rbt_period_ms_ctr = 0;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_turn_right_20deg      （大）右折する（センサー状態がRBT_SNS_L_LEFTの状態である）
 *
 *  書　式 :   void f_turn_right_20deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state)
 *              st_line_tracer_state_t* nxt_state           次の状態のメモリ番地
 *              const st_line_tracer_state_t* cur_state     現在の状態のメモリ番地
 *
 *  機　能 :   1. 停止状態からだと、右タイヤを最低速度の周期+2、左タイヤを最低速度の周期-1で走らせる
 *            2. 走っている状態からだと、右タイヤを速い方を持つタイヤの速度、左タイヤをその速度の周期+3で走る
 *            3. ロボットがとっても速い速度で走ると（周期2ms）、（中）右折を行う
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
static void f_turn_right_20deg(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state) {

    unsigned char tmp_spd;

    /* 
        If changing to RBT_SNS_S_RIGHT when moving, maintain the speed while turning.
        If changing from RBT_MOV_STP, initiate with 2ms and 5ms.
    */
    nxt_state->rbt_movement_state = RBT_MOV_TRN_RIGHT_20DEG;
    if (cur_state->rbt_movement_state == RBT_MOV_STP) {
        nxt_state->rbt_speed_timer_l_wheel = g_line_tracer_state_min_speed_period - 1;
        nxt_state->rbt_speed_timer_r_wheel = g_line_tracer_state_min_speed_period + 2;
    } 
    /* Use the smallest period */
    else {
        tmp_spd = (cur_state->rbt_speed_timer_l_wheel < cur_state->rbt_speed_timer_r_wheel && cur_state->rbt_speed_timer_l_wheel) ?
                cur_state->rbt_speed_timer_l_wheel : cur_state->rbt_speed_timer_r_wheel;

        if (tmp_spd <= 2) {
            nxt_state->rbt_speed_timer_l_wheel = tmp_spd;
            nxt_state->rbt_speed_timer_r_wheel = tmp_spd + 2;
        } else if (tmp_spd - 1 >= g_line_tracer_state_max_speed_period) {
            nxt_state->rbt_speed_timer_l_wheel = tmp_spd - 1;
            nxt_state->rbt_speed_timer_r_wheel = tmp_spd + 2;
        } else {
            nxt_state->rbt_speed_timer_l_wheel = tmp_spd;
            nxt_state->rbt_speed_timer_r_wheel = tmp_spd + 3;
        }
    }

    nxt_state->rbt_period_ms_ctr = 0;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_do_nothing      特に何もしない
 *
 *  書　式 :   void f_do_nothing(void)
 *
 *  機　能 :   何もしない
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
static void f_do_nothing() {
    /* nop */
}

/***********************************************************************

                    Sensor Checks

************************************************************************/

static uint8_t f_is_middle(st_line_tracer_state_t* state) {
    return state->rbt_sensor_state == RBT_SNS_MIDDLE;
}
static uint8_t f_is_s_left(st_line_tracer_state_t* state) {
    return state->rbt_sensor_state == RBT_SNS_S_LEFT;
}
static uint8_t f_is_m_left(st_line_tracer_state_t* state) {
    
    return state->rbt_sensor_state == RBT_SNS_M_LEFT;
}
static uint8_t f_is_l_left(st_line_tracer_state_t* state) {
    
    return state->rbt_sensor_state == RBT_SNS_L_LEFT;
}
static uint8_t f_is_s_right(st_line_tracer_state_t* state) {
    
    return state->rbt_sensor_state == RBT_SNS_S_RIGHT;
}
static uint8_t f_is_m_right(st_line_tracer_state_t* state) {
    
    return state->rbt_sensor_state == RBT_SNS_M_RIGHT;
}
static uint8_t f_is_l_right(st_line_tracer_state_t* state) {
    
    return state->rbt_sensor_state == RBT_SNS_L_RIGHT;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_hard_shutdown      例外時対策. ハードシャットダウン.
 *
 *  書　式 :   void f_hard_shutdown(void)
 *
 *  機　能 :   例外時対策. ハードシャットダウン.
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
static void f_hard_shutdown(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state) {
    /* Stop movement  */
    nxt_state->rbt_sensor_state = cur_state->rbt_sensor_state;
    nxt_state->rbt_movement_state = RBT_MOV_STP;
    nxt_state->rbt_speed_timer_l_wheel = 0;
    nxt_state->rbt_speed_timer_r_wheel = 0;
    nxt_state->rbt_period_ms_ctr = 0;

    /* reset */
    set_fix_mid_0();
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   f_hard_shutdown      例外時対策. ソフトシャットダウン.
 *
 *  書　式 :   void f_hard_shutdown(void)
 *
 *  機　能 :   例外時対策. ソフトシャットダウン.
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
static void f_soft_shutdown(st_line_tracer_state_t* nxt_state, const st_line_tracer_state_t* cur_state) {

    unsigned char tmp_spd;

    /* Do deceleration for each wheel */
    tmp_spd = (cur_state->rbt_speed_timer_l_wheel < cur_state->rbt_speed_timer_r_wheel && cur_state->rbt_speed_timer_l_wheel) ?
        cur_state->rbt_speed_timer_l_wheel : cur_state->rbt_speed_timer_r_wheel;

    if (tmp_spd >= g_line_tracer_state_min_speed_period - 1) {
        nxt_state->rbt_speed_timer_r_wheel = g_line_tracer_state_min_speed_period;
        nxt_state->rbt_speed_timer_l_wheel = g_line_tracer_state_min_speed_period;
        nxt_state->rbt_movement_state = RBT_MOV_SOFT_SHUTDOWN;
    }
    else {
        nxt_state->rbt_speed_timer_r_wheel = tmp_spd + 1;
        nxt_state->rbt_speed_timer_l_wheel = tmp_spd + 1;       
    }
    nxt_state->rbt_movement_state = RBT_MOV_SOFT_SHUTDOWN;
    nxt_state->rbt_period_ms_ctr = 0;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   rotate_l            左回転
 *
 *  書　式 :   uint8_t rotate_l(uint8_t data)
 *              uint8_t data;       データ
 *
 *  機　能 :   1.データを１ビット左に回転する
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

uint8_t rotate_l(uint8_t data)
{
    uint8_t msb2lsb;

    msb2lsb = data >> CHAR_BIT - 1; /* charは8ビットとは限らない：limits.h参照 */    /* Fill in the blanks! */
    data <<= 1;
    data |= msb2lsb;

    return data;
}
/*------------------ end of rotate_l() ----------------------------------------*/

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   rotate_r            右回転
 *
 *  書　式 :   uint8_t rotate_r(uint8_t data)
 *              uint8_t data;       データ
 *
 *  機　能 :   1.データを１ビット右に回転する
 *
 *+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

uint8_t rotate_r(uint8_t data)
{
    uint8_t lsb2msb;

    lsb2msb = data << CHAR_BIT - 1; /* charは8ビットとは限らない：limits.h参照 */    /* Fill in the blanks! */
    data >>= 1;
    data |= lsb2msb;

    return data;
}
/*------------------ end of rotate_r() ----------------------------------------*/
