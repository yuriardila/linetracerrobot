;/******************************************************************************
; *
; *	ƒVƒXƒeƒ€–¼F@ƒXƒ^[ƒ^ƒLƒbƒg—pƒXƒ^[ƒgƒAƒbƒvƒ‹[ƒ`ƒ“@iMN101C49K—pj
; *			
; *
; *	Copyright (c) Panasonic Corporation 2003-2014
; *
; *-----------------------------------------------------------------------------
; *
; *	$RCSfile: startup.asm,v $	$Revision: 1.1 $
; *
; *	$Ý@Œv: X–{‘×Ži		$“ú•t: 2003/ 7/17
; *	$Author: Y.Morimoto $		$Date: 2014-08-29 15:46:42+09 $
; *
; ******************************************************************************/

#include "general.ah"
#include "mn101c49.ah"
#include "trainer.ah"

; === ƒZƒOƒƒ“ƒg’è‹` ===

_STEXT		SECTION	CODE,PUBLIC,0
_CONST		SECTION	CODE,PUBLIC,1
_GCONST		SECTION	CODE,PUBLIC,1
_DATA		SECTION	DATA,PUBLIC,1
_GDATA		SECTION	DATA,PUBLIC,1
_ROMDATA	SECTION	CODE,PUBLIC,1
_GROMDATA	SECTION	CODE,PUBLIC,1
_ROMDATAEND	SECTION	CODE,PUBLIC,1
_ENTRY		SECTION CODE,PUBLIC,0
_TEXT		SECTION	CODE,PUBLIC,0
_BSS		SECTION	DATA,PUBLIC,1
_GBSS		SECTION	DATA,PUBLIC,1
_BSSEND		SECTION	DATA,PUBLIC,1

; === Š„ž‚ÝƒxƒNƒ^iƒXƒ^[ƒ^ƒLƒbƒg—pj ===

	GLOBAL	_main
	GLOBAL	_tmint_1ms

_STEXT		SECTION		; Specifies -T_STEXT=5808 by linker

	jmp	reset		; Sets reset vector
	org	0x04
	jmp	dummy		; #01:NMI
	org	0x08
	jmp	dummy		; #02:IRQ0
	org	0x0c
	jmp	dummy		; #03:IRQ1
	org	0x10
	jmp	dummy		; #04:IRQ2
	org	0x14
	jmp	dummy		; #05:IRQ3
	org	0x18
	jmp	dummy		; #06:IRQ4
	org	0x1c
	jmp	dummy		; #07:IRQ5
	org	0x20
	jmp	dummy		; #08:Reserved
	org	0x24
	jmp	dummy		; #09:TM0IRQ
	org	0x28
	jmp	_tmint_1ms	; #10:TM1IRQ
	org	0x2c
	jmp	dummy		; #11:TM2IRQ
	org	0x30
	jmp	dummy		; #12:TM3IRQ
	org	0x34
	jmp	dummy		; #13:TM4IRQ
	org	0x38
	jmp	dummy		; #14:Reserved
	org	0x3c
	jmp	dummy		; #15:TM6IRQ
	org	0x40
	jmp	dummy		; #16:TBIRQ
	org	0x44
	jmp	dummy		; #17:TM7IRQ
	org	0x48
	jmp	dummy		; #18:TM7OC2IRQ
	org	0x4c
	jmp	dummy		; #19:Reserved
	org	0x50
	jmp	dummy		; #20:Reserved
	org	0x54
	jmp	dummy		; #21:SC0RIRQ
	org	0x58
	jmp	dummy		; #22:SC0TIRQ
	org	0x5c
	jmp	dummy		; #23:SC1IRQ
	org	0x60
	jmp	dummy		; #24:Reserved (monitor SC2IRQ)
	org	0x64
	jmp	dummy		; #25:SC3IRQ
	org	0x68
	jmp	dummy		; #26:ADIRQ
	org	0x6c
	jmp	dummy		; #27:Reserved
	org	0x70
	jmp	dummy		; #28:ATC1IRQ
	org	0x74
	jmp	dummy		; #29:Reserved
	org	0x78
	jmp	dummy		; #30:Reserved

	org	0x80		; To location _STEXT + 0x80 (0x5888)

_TEXT		SECTION
; === ƒ_ƒ~[iŠ„ž‚Ý—pj ===

dummy
	rti			; Function that does nothing other than return

;/*----------------------------------------------------------------------------*/
_TEXT		SECTION
; === Power on reset ===

reset
; === disable maskable interrupt (to make sure) ===
	and	~(PSW_MIE | PSW_LV3),PSW

; === initialize stack pointer ===
	movw	USR_STACK_P,A0
	movw	A0,SP

init_cpu
; === initialize CPU mode ===
	mov	0 + CPUM_NORMAL | CPUM_OSCNORMAL | CPUM_OSCSEL11,(CPUM)
; === initialize slow clock mode ===
	mov	0 + OSCMD_SLWOSCNOR,(OSCMD)
; === initialize memory mode ===
	mov	0 + MEMCTR_EXMEM0 | MEMCTR_EXW3 | MEMCTR_EXWFW | MEMCTR_IVBM4000 | MEMCTR_IOW0,(MEMCTR)
	mov	0 + EXADV_11_8DIS | EXADV_15_12DIS | EXADV_17_16DIS,(EXADV)
; === initialize waiting time for stable oscillation ===
	mov	0 + DLYCTR_14FS | DLYCTR_P6OUT,(DLYCTR)
; === initialize memory banks ===
	mov	0 + BNKR_0,(SBNKR)
	mov	0 + BNKR_0,(DBNKR)
; === initialize watchdog mode ===
	mov	0 + WDCTR_WDDIS,(WDCTR)

; === check RAM ===
	movw	USR_RAM_TOP,A0
	movw	USR_RAM_END,A1
	movw	0x5555,DW0	; with 0x5555 pattern at first
lp_write1
	movw	DW0,(A0)	; write every two bytes
	addw	2,A0
	cmpw	A1,A0
	bcs	lp_write1

	movw	USR_RAM_TOP,A0
lp_chk1
	movw	(A0),DW0	; check every two bytes
	cmpw	0x5555,DW0
	bne	lp_chk1		; if the data doesn't match, loop here forever
	addw	2,A0
	cmpw	A1,A0
	bcs	lp_chk1

	movw    USR_RAM_TOP,A0
	movw    0xAAAA,DW0	; then with 0xAAAA pattern
lp_write2
	movw	DW0,(A0)	; write every two bytes
	addw	2,A0
	cmpw	A1,A0
	bcs	lp_write2

	movw	USR_RAM_TOP,A0
lp_chk2
	movw	(A0),DW0	; check every two bytes
	cmpw	0xAAAA,DW0
	bne	lp_chk2		;  if the data doesn't match, loop here forever
	addw	2,A0
	cmpw	A1,A0
	bcs	lp_chk2

; === clear RAM ===
	movw	USR_RAM_TOP,A0
	movw	0x0000,DW0
lp_clr
	movw	DW0,(A0)	; clear two bytes at a time
	addw	2,A0
	cmpw	USR_RAM_END,A0
	bcs	lp_clr

	jsr	init_var	; •Ï”‚Ì‰Šú‰»
	jsr	init_port	; ƒ|[ƒg‚Ì‰Šú‰»
	jsr	init_int	; Š„ž‚Ý‚Ì‰Šú‰»

	jsr	_main		; mainŠÖ”‚ÌŒÄ‚Ño‚µ
forever
	bra	forever

;/*----------------------------------------------------------------------------*/
;/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; *
; *	ŠÖ”–¼:	init_var	•Ï”‚Ì‰Šú‰»
; *
; *	‘@Ž®:	void init_var(void)
; *
; *	‹@@”\:	1.‰Šú‰»Žq‚Â‚«‚ÌŠO•”•Ï”AÃ“I•Ï”‚ð‰Šú‰»‚·‚é
; *
; *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

init_var
; === initialize the static variables ===
init_static
	movw	_ROMDATA,A0
	cmpw	_GROMDATA,A0	; Ã“I•Ï”‚ª‚È‚¯‚ê‚ÎƒXƒLƒbƒv
	beq	init_global

	movw	_DATA,A1
lp_init_static			; ROMDATA(A0) -> DATA(A1)
	mov	(A0),D0
	mov	D0,(A1)
	addw	0x1,A0
	addw	0x1,A1
	cmpw	_GROMDATA,A0	; _GROMDATA‚Ì‘O‚Ü‚Å
	bne	lp_init_static

; === initialize the global variables ===
init_global
	movw	_GROMDATA,A0
	cmpw	_ROMDATAEND,A0	; ŠO•”•Ï”‚ª‚È‚¯‚ê‚ÎƒXƒLƒbƒv
	beq	end_init_var
	movw	_GDATA,A1

lp_init_global			; GROMDATA(A0) -> GDATA(A1)
	mov	(A0),D0
	mov	D0,(A1)
	addw	0x1,A0
	addw	0x1,A1
	cmpw	_ROMDATAEND,A0	; _ROMDATAEND‚Ì‘O‚Ü‚Å
	bne	lp_init_global

end_init_var
	rts

;/*------------------ end of init_var() ---------------------------------------*/
; *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
; *
; *	ŠÖ”–¼:	init_port	   ƒ|[ƒg‚Ì‰Šú‰»
; *
; *	‘@Ž®:	void init_port(void)
; *
; *	‹@  ”\:	1.ƒ|[ƒg‚S‚ð“ü—ÍiƒgƒOƒ‹SW“ü—Íj‚ÉÝ’è‚·‚é
; *			“ü—ÍƒI[ƒvƒ“‚Ì‰Â”\«‚ª‚ ‚é‚Ì‚Åƒvƒ‹ƒAƒbƒv•K—v
; *		2.ƒ|[ƒg‚PiLEDj‚ðo—Í‚ÉÝ’è‚·‚é
; *		3.ƒ|[ƒg‚T‚Æ‚UiLCD§Œäj‚ðo—Í‚ÉÝ’è‚·‚é
; *		4.ƒ|[ƒg‚WiLCDƒf[ƒ^j‚ðŽæ‚è‚ ‚¦‚¸“ü—ÍÝ’èi‘o•ûŒüƒoƒXj
; *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

init_port
; === initialize port 4 ===
	mov	0x00,(P4IMD)		; ƒL[Š„ž‚Ý‚É‚µ‚È‚¢iƒfƒtƒHƒ‹ƒgF”O‚Ì‚½‚ßj
	mov	0 + FLOAT_P4RUP,(FLOAT)	; ƒvƒ‹ƒAƒbƒv‘I‘ðiƒfƒtƒHƒ‹ƒgF”O‚Ì‚½‚ßj
	mov	0xff,(P4PLUD)		; ƒvƒ‹ƒAƒbƒv’ïR‚ ‚è
	mov	0x00,(P4DIR)		; “ü—ÍÝ’è

; === initialize port 1 ===
	mov	0x00,(P1OMD)		; ’ÊíIOiƒ^ƒCƒ}IO‚Å‚È‚­j
	mov	0x00,(P1TCNT)		; ’ÊíIOiƒŠƒAƒ‹ƒ^ƒCƒ€§Œä‚Å‚Í‚È‚­j
	mov	0x00,(P1OUT)		; ”ñƒAƒNƒeƒBƒuƒf[ƒ^
	mov	0xff,(P1DIR)		; o—ÍÝ’è
	mov	0x00,(P1PLU)		; ƒvƒ‹ƒAƒbƒv’ïR‚È‚µ

; === initialize port 3 ===
	mov	0x00,(P3OUT)		; ”ñƒAƒNƒeƒBƒuƒf[ƒ^
	mov	0xff,(P3DIR)		; o—ÍÝ’è
	mov	0x00,(P3PLU)		; ƒvƒ‹ƒAƒbƒv’ïR‚È‚µ

; === initialize port 5 ===
	mov	0x00,(P5OUT)		; ”ñƒAƒNƒeƒBƒuƒf[ƒ^iLCD‚Í“ü—Íj
	mov	0xff,(P5DIR)		; o—ÍÝ’è
	mov	0x00,(P5PLU)		; ƒvƒ‹ƒAƒbƒv’ïR‚È‚µ

; === initialize port 6 ===
	mov	0x00,(P6OUT)		; ”ñƒAƒNƒeƒBƒuƒf[ƒ^
	mov	0xff,(P6DIR)		; o—ÍÝ’è
	mov	0x00,(P6PLU)		; ƒvƒ‹ƒAƒbƒv’ïR‚È‚µ

; === initialize port 8 ===
	mov	0xff,(P8PLU)		; ƒvƒ‹ƒAƒbƒv’ïR‚ ‚è
	mov	0x00,(P8DIR)		; Žæ‚è‚ ‚¦‚¸“ü—ÍÝ’èi‘o•ûŒüƒoƒXj

; === initialize port A ===
	mov	0x00,(PAIMD)		; ”ñƒAƒNƒeƒBƒuƒf[ƒ^
	mov	0 + FLOAT_PARUP,(FLOAT)	; ƒvƒ‹ƒAƒbƒv‘I‘ðiƒfƒtƒHƒ‹ƒgF”O‚Ì‚½‚ßj
	mov	0x00,(PAPLUD)		; ƒvƒ‹ƒAƒbƒv’ïR‚È‚µ

; === initialize port D ===
	mov	0x00,(PDOUT)		; ”ñƒAƒNƒeƒBƒuƒf[ƒ^
	mov	0xff,(PDDIR)		; o—ÍÝ’è
	mov	0x00,(PDPLU)		; ƒvƒ‹ƒAƒbƒv’ïR‚È‚µ


	rts

;/*------------------ end of init_port() --------------------------------------*/
; *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
; *
; *	ŠÖ”–¼:	init_int		Š„ž‚Ý‚Ì‰Šú‰»
; *
; *	‘@Ž®:	void init_int (void)
; *
; *	‹@  ”\:	1.ƒ^ƒCƒ}‚OA‚P‚ð‚Pms‚ÌŠî€ƒ^ƒCƒ}‚ÉÝ’è‚·‚é
; *			ƒJƒXƒP[ƒhÚ‘±‚Æ‚µAŠ„ž‚ÝƒŒƒxƒ‹‚Í‚P‚Æ‚·‚é
; *		2.ŠO•”Š„‚èž‚Ý‚OiŠ„ž‚ÝƒŒƒxƒ‹‚Qj‚ð
; *			ƒmƒCƒYƒtƒBƒ‹ƒ^‚ ‚è‚ÅÝ’è‚·‚éi‚r‚v‚Qj
; *		2.ŠO•”Š„‚èž‚Ý‚PiŠ„ž‚ÝƒŒƒxƒ‹‚Qj‚ð
; *			ƒmƒCƒYƒtƒBƒ‹ƒ^‚ ‚è‚ÅÝ’è‚·‚éi‚r‚v‚Rj
; *		4.Š„ž‚Ýƒ}ƒXƒNƒŒƒxƒ‹‚R‚ðÝ’è‚·‚éiŠ„ž‚Ý‚Í‹ÖŽ~‚Ì‚Ü‚Üj
; *
; *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

init_int
; === initialize timer 0,1 ===
	mov	0 + TM0MD_TM0PSC | TM0MD_STOP | TM0MD_NORMAL | TM0MD_PWNORMAL,(TM0MD)
	mov	0 + TM1MD_CAS | TM1MD_STOP | TM1MD_NORMAL,(TM1MD)
	mov	0 + CK0MD_FS_4,(CK0MD)		; fs/4 = 400ns
	mov	0 + PSCMD_EN,(PSCMD)		; ƒvƒŠƒXƒP[ƒ‰“®ì
	movw	0 + (2500 - 1),DW0		; 1msi2500 = 1ms / 400nsj
;								int(2500*19.6608MHz/20MHz+0.5)-1
	movw	DW0,(TM0OC)
	bset	(MEMCTR)BP_IRWEN		; Š„ž‚Ý—v‹ƒtƒ‰ƒOiIRj‚ÌƒNƒŠƒA‹–‰Â
	mov	0 + ICR_ID | ICR_LV3,(TM0ICR)	; ”O‚Ì‚½‚ßFŠ„ž‚Ý‚Í‚È‚¢‚Í‚¸
	mov	0 + ICR_IE | ICR_LV1,(TM1ICR)	; Š„ž‚Ý—v‹ƒtƒ‰ƒOiIRj‚ÍƒNƒŠƒA
	bset	(TM1MD)BP_TM_RUN		; ƒ^ƒCƒ}‹ì“®iãˆÊ‚©‚çj
	bset	(TM0MD)BP_TM_RUN		; ƒ^ƒCƒ}‹ì“®

; === initialize IRQ 0, 1 ===
	mov	0 + NFCTR_0ON | NFCTR_0FOSC_10 | NFCTR_1ON | NFCTR_1FOSC_10,(NFCTR)	; ƒmƒCƒYƒtƒBƒ‹ƒ^[‚ ‚è
	mov	0 + ICR_IE | ICR_FEDGE | ICR_LV2,(IRQ0ICR)	; Š„ž‚Ý—v‹ƒtƒ‰ƒOiIRj‚ÍƒNƒŠƒA
	mov	0 + ICR_IE | ICR_FEDGE | ICR_LV2,(IRQ1ICR)	; Š„ž‚Ý—v‹ƒtƒ‰ƒOiIRj‚ÍƒNƒŠƒA
	bclr	(MEMCTR)BP_IRWEN		; Š„ž‚Ý—v‹ƒtƒ‰ƒOiIRj‚ÌƒNƒŠƒA‹ÖŽ~

; === set intrrupt mask level ===
	or	0 + PSW_LV3,PSW

	rts

;/*------------------ end of init_int() ---------------------------------------*/

_CONST		SECTION
	dc	"$Id: startup.asm,v 1.1 2014-08-29 15:46:42+09 Y.Morimoto Exp $"

	end

;/*------------------ end of startup.asm --------------------------------------*/
