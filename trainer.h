/*******************************************************************************
 *
 *		システム名：　スタータキット用ヘッダファイル （MN101C49K用）
 *
 *		Copyright (c) Panasonic Corporation 2003-2014
 *
 *------------------------------------------------------------------------------
 *
 *		$RCSfile: trainer.h,v $		$Revision: 1.2 $
 *
 *		$設　計: 森本泰司			$日付: 2003/ 7/17
 *		$Author: Y.Morimoto $		$Date: 2014-08-24 17:28:57+09 $
 *
 *******************************************************************************/

#ifndef _TRAINER_H
#define _TRAINER_H

#include "general.h"
#include "mn101c49.h"
#include "lcd.h"

#define LED			P1OUT

#define TSW			P4IN
#define TSW_0		BIT_PATTERN_0
#define TSW_7       BIT_PATTERN_7

#define RWH         P3OUT
#define LWH         PDOUT

#define SNS         PAIN

#define LCD_DATA_R  P8IN
#define LCD_DATA_W  P8OUT
#define LCD_DB_DIR  P8DIR
#define LCD_DB_PLU  P8PLU
#define SET_INPUT   (0x00U)
#define SET_OUTPUT  (0xffU)
#define RST_PULLUP  (0x00U)
#define SET_PULLUP  (0xffU)

#define LCD_RW_CTRL P5OUT
#define LCD_RS_SEL  P6OUT

#endif	/* _TRAINER_H */

/*------------------ end of trainer.h -----------------------------------------*/
