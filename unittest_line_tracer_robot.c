/*******************************************************************************
 *
 *      システム名：　ラインとレーサーロボット
 *
 *		Copyright (c) Yuri Ardila 2015
 *
 *-----------------------------------------------------------------------------
 *
 *		$RCSfile: unittest_line_tracer_robot.c,v $		$Revision: 1.0 $
 *
 *		$設　計: Yuri Ardila			$日付: 2015/06/06
 *		$Author: Yuri Ardila $		$Date: 2015-06-06 17:08:02+09 $
 *
 *******************************************************************************/

#include "unittest_line_tracer_robot.h"

/*-------------------------- UTILS -----------------------------------*/

#define abs(x) (((x) < 0) ? -(x) : (x))

/* 関数プロトタイプ宣言 */
static char *reverse2(char *s);

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   itoa2    数値 --->文字列変換
 *
 *  書　式 :   char *itoa2(int n, char *s, int w)
 *              int n;      変換したい数値データ
 *              char *s;    結果を格納する先頭番地
 *              int w;      変換最小幅
 *
 *  機　能 :   1.数値 ---> 文字列変換して最小幅に満たない場合は、
 *                          空白を詰める。
 *
 *  結　果 :   関数値：結果を格納した文字列の先頭番地
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

char *itoa2(int n, char *s, int w)       /* K&R p.77,78参照 */
{
    int i, sign;

    sign = n;                       /* 符号を記憶 */

    i = 0;
    do {
        s[i++] = abs(n % 10) + '0';
    } while ((n /= 10) != 0);

    if (sign < 0) {
        s[i++] = '-';
    }

    while (i < w) {                 /* 最小幅に満たない場合 */
        s[i++] = ' ';
    }
    s[i] = '\0';

    return reverse2(s);
}
/*------------------ end of itoa2() -----------------------------------*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *
 *  関数名 :   reverse2     文字列を逆順にする
 *
 *  書　式 :   char *reverse2(char *s)
 *              char *s;    結果を格納する先頭番地
 *
 *  機　能 :   1.文字列を逆順にする。
 *
 *  結　果 :   関数値：結果を格納した文字列の先頭番地
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

static char *reverse2(char *s)               /* K&R p.76参照 */
{
    int c, i, j;

    for (i = 0, j = strlen((const char *)s) - 1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }

    return s;
}
/*------------------ end of reverse2() ---------------------------------*/

/*-------------------------- END OF UTILS ------------------------------*/


int unittest_ltr_move_l_wheel() {

    uint8_t buf_lwh = 0x99;

    unsigned int chk_5ms;
    unsigned int buf_chk_5ms;

    unsigned int chk_1s;
    unsigned int buf_chk_1s;

    reset_timers();

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    chk_5ms = get_time_5ms();
    buf_chk_5ms = chk_5ms;

    FOREVER {

        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
        }

        if (buf_chk_1s >= 1) {
            break;
        }

        chk_5ms = get_time_5ms();
        if (buf_chk_5ms != chk_5ms) {
            buf_chk_5ms = chk_5ms;
            buf_lwh = rotate_r(buf_lwh);
            outp(LWH, buf_lwh);
        }
    }

    return 0;
}

int unittest_ltr_move_r_wheel() {

    uint8_t buf_rwh = 0x99;

    unsigned int chk_5ms;
    unsigned int buf_chk_5ms;

    unsigned int chk_1s;
    unsigned int buf_chk_1s;

    reset_timers();

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    chk_5ms = get_time_5ms();
    buf_chk_5ms = chk_5ms;

    FOREVER {


        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
        }

        if (buf_chk_1s >= 1) {
            break;
        }

        chk_5ms = get_time_5ms();
        if (buf_chk_5ms != chk_5ms) {
            buf_chk_5ms = chk_5ms;
            buf_rwh = rotate_l(buf_rwh);
            outp(RWH, buf_rwh);
        }
    }

    return 0;
}

int unittest_ltr_move_both_wheel() {

    uint8_t buf_rwh = 0x99;
    uint8_t buf_lwh = 0x99;

    unsigned int chk_5ms;
    unsigned int buf_chk_5ms;

    unsigned int chk_1s;
    unsigned int buf_chk_1s;

    reset_timers();

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    chk_5ms = get_time_5ms();
    buf_chk_5ms = chk_5ms;

    FOREVER {

        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
        }

        if (buf_chk_1s >= 1) {
            break;
        }

        chk_5ms = get_time_5ms();
        if (buf_chk_5ms != chk_5ms) {
            buf_chk_5ms = chk_5ms;

            outp(LWH, buf_lwh); outp(RWH, buf_rwh);
            buf_lwh = rotate_r(buf_lwh); buf_rwh = rotate_l(buf_rwh);
        }
    }

    return 0;
}

int unittest_ltr_move_l_wheel_with_period(int period) {

    uint8_t buf_lwh = 0x99;

    unsigned int chk_1ms;
    unsigned int buf_chk_1ms;

    unsigned int chk_1s;
    unsigned int buf_chk_1s;

    unsigned int cp_period;
    cp_period = period;

    reset_timers();

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    chk_1ms = get_time_1ms();
    buf_chk_1ms = chk_1ms;

    FOREVER {

        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
        }

        if (buf_chk_1s >= 1) {
            break;
        }

        chk_1ms = get_time_1ms();
        if (buf_chk_1ms != chk_1ms) {
            buf_chk_1ms = chk_1ms;
            cp_period--;
        }

        if (cp_period <= 0) {
            buf_lwh = rotate_r(buf_lwh);
            outp(LWH, buf_lwh);

            cp_period = period;
        }  
    }

    return 0;
}


int unittest_ltr_move_r_wheel_with_period(int period) {
    
    uint8_t buf_rwh = 0x99;

    unsigned int chk_1ms;
    unsigned int buf_chk_1ms;

    unsigned int chk_1s;
    unsigned int buf_chk_1s;

    unsigned int cp_period;
    cp_period = period;

    reset_timers();

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    chk_1ms = get_time_1ms();
    buf_chk_1ms = chk_1ms;

    FOREVER {

        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
        }

        if (buf_chk_1s >= 1) {
            break;
        }

        chk_1ms = get_time_1ms();
        if (buf_chk_1ms != chk_1ms) {
            buf_chk_1ms = chk_1ms;
            cp_period--;
        }

        if (cp_period <= 0) {
            buf_rwh = rotate_l(buf_rwh);
            outp(RWH, buf_rwh);

            cp_period = period;
        }  
    }

    return 0;
}

int unittest_ltr_move_both_wheel_with_period(int period) {

    uint8_t buf_rwh = 0x99;
    uint8_t buf_lwh = 0x99;

    unsigned int chk_1ms;
    unsigned int buf_chk_1ms;

    unsigned int chk_1s;
    unsigned int buf_chk_1s;

    unsigned int cp_period;
    cp_period = period;

    reset_timers();

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    chk_1ms = get_time_1ms();
    buf_chk_1ms = chk_1ms;

    FOREVER {

        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
        }

        if (buf_chk_1s >= 1) {
            break;
        }

        chk_1ms = get_time_1ms();
        if (buf_chk_1ms != chk_1ms) {
            buf_chk_1ms = chk_1ms;
            cp_period--;
        }

        if (cp_period <= 0) {
            buf_rwh = rotate_l(buf_rwh);
            outp(RWH, buf_rwh);

            buf_lwh = rotate_r(buf_lwh);
            outp(LWH, buf_lwh);

            cp_period = period;
        }  
    }

    return 0;
}

int unittest_ltr_check_lcd(char* str) {

    unsigned int chk_1s;
    unsigned int buf_chk_1s;

    reset_timers();

    lcd_puts_line_loc(str, 0, 0);

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    FOREVER {
        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
        }

        if (buf_chk_1s >= 5) {
            break;
        }
    }
    
    return 0;
}

int unittest_ltr_check_lcd_counter_1s() {

    unsigned int chk_1s;
    unsigned int buf_chk_1s;

    char hyoji[80];

    reset_timers();

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    FOREVER {
        /* Increment counter every second */
        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
            itoa2(buf_chk_1s, hyoji, 2);
            lcd_puts_line_loc(hyoji, 0, 0);
        }

        if (buf_chk_1s >= 10) {
            break;
        }
    }
    
    return 0;
}

int unittest_ltr_check_sensor() {

    unsigned int chk_1s;
    unsigned int buf_chk_1s;

    char hyoji[80];
    unsigned char cur_sns;

    reset_timers();

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    FOREVER {
        cur_sns = get_sns();

        /* Increment counter every second */
        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
            itoa2(cur_sns, hyoji, 2);
            lcd_puts_line_loc(hyoji, 0, 0);
        }

        if (buf_chk_1s >= 10) {
            break;
        }
    }

    return 0;
}


int unittest_ltr_move_on_sns_middle() {

    uint8_t buf_rwh = 0x99;
    uint8_t buf_lwh = 0x99;

    unsigned int chk_1s;
    unsigned int buf_chk_1s;

    unsigned int chk_2ms;
    unsigned int buf_chk_2ms;

    unsigned char cur_sns;
    unsigned char buf_sns;
    uint8_t rbt_mov_state = RBT_MOV_STP;
    uint8_t rbt_sns_state = RBT_SNS_UNKNOWN;

    reset_timers();

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    chk_2ms = get_time_2ms();
    buf_chk_2ms = chk_2ms;

    FOREVER {
        
        cur_sns = get_sns();
        
        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
        }

        if (buf_chk_1s >= 10) {
            break;
        }

        if (cur_sns != buf_sns) {
            buf_sns = cur_sns;
            
            if (buf_sns == RBT_SNS_MIDDLE) {
                rbt_mov_state = RBT_MOV_RUN_SPEED_2MS;
            }

            else {
                rbt_mov_state = RBT_MOV_STP;
            }

        }

        chk_2ms = get_time_2ms();
        if (buf_chk_2ms != chk_2ms) {
            buf_chk_2ms = chk_2ms;

            if (rbt_mov_state == RBT_MOV_RUN_SPEED_2MS) {
                outp(LWH, buf_lwh); outp(RWH, buf_rwh);
                buf_lwh = rotate_r(buf_lwh); buf_rwh = rotate_l(buf_rwh);                
            }
        }
    }


    return 0;
}

int unittest_ltr_move_on_sns_s_left() {

    uint8_t buf_rwh = 0x99;
    uint8_t buf_lwh = 0x99;

    unsigned int chk_1s;
    unsigned int buf_chk_1s;

    unsigned int chk_5ms;
    unsigned int buf_chk_5ms;

    unsigned char cur_sns;
    unsigned char buf_sns;
    uint8_t rbt_mov_state = RBT_MOV_STP;
    uint8_t rbt_sns_state = RBT_SNS_UNKNOWN;

    reset_timers();

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    chk_5ms = get_time_5ms();
    buf_chk_5ms = chk_5ms;

    FOREVER {
        
        cur_sns = get_sns();
    
        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
        }

        if (buf_chk_1s >= 10) {
            break;
        }

        if (cur_sns != buf_sns) {
            buf_sns = cur_sns;
            
            if (buf_sns == RBT_SNS_S_LEFT) {
                rbt_mov_state = RBT_MOV_TRN_RIGHT_5DEG;
            }

            else {
                rbt_mov_state = RBT_MOV_STP;
            }

        }

        chk_5ms = get_time_5ms();
        if (buf_chk_5ms != chk_5ms) {
            buf_chk_5ms = chk_5ms;

            if (rbt_mov_state == RBT_MOV_TRN_RIGHT_5DEG) {
                outp(LWH, buf_lwh);
                buf_lwh = rotate_r(buf_lwh);                
            }
        }
    }


    return 0;
}

int unittest_ltr_move_on_sns_m_left() {

    uint8_t buf_rwh = 0x99;
    uint8_t buf_lwh = 0x99;

    unsigned int chk_1s;
    unsigned int buf_chk_1s;

    unsigned int chk_4ms;
    unsigned int buf_chk_4ms;

    unsigned char cur_sns;
    unsigned char buf_sns;
    uint8_t rbt_mov_state = RBT_MOV_STP;
    uint8_t rbt_sns_state = RBT_SNS_UNKNOWN;

    reset_timers();

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    chk_4ms = get_time_4ms();
    buf_chk_4ms = chk_4ms;

    FOREVER {
        
        cur_sns = get_sns();

        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
        }

        if (buf_chk_1s >= 10) {
            break;
        }

        if (cur_sns != buf_sns) {
            buf_sns = cur_sns;
            
            if (buf_sns == RBT_SNS_M_LEFT) {
                rbt_mov_state = RBT_MOV_TRN_RIGHT_10DEG;
            }

            else {
                rbt_mov_state = RBT_MOV_STP;
            }

        }

        chk_4ms = get_time_4ms();
        if (buf_chk_4ms != chk_4ms) {
            buf_chk_4ms = chk_4ms;

            if (rbt_mov_state == RBT_MOV_TRN_RIGHT_10DEG) {
                outp(LWH, buf_lwh);
                buf_lwh = rotate_r(buf_lwh);                
            }
        }
    }

    return 0;
}

int unittest_ltr_move_on_sns_l_left() {
    
    uint8_t buf_rwh = 0x99;
    uint8_t buf_lwh = 0x99;

    unsigned int chk_1s;
    unsigned int buf_chk_1s;

    unsigned int chk_2ms;
    unsigned int buf_chk_2ms;

    unsigned char cur_sns;
    unsigned char buf_sns;
    uint8_t rbt_mov_state = RBT_MOV_STP;
    uint8_t rbt_sns_state = RBT_SNS_UNKNOWN;

    reset_timers();

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    chk_2ms = get_time_2ms();
    buf_chk_2ms = chk_2ms;

    FOREVER {
        
        cur_sns = get_sns();

        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
        }

        if (buf_chk_1s >= 10) {
            break;
        }

        if (cur_sns != buf_sns) {
            buf_sns = cur_sns;
            
            if (buf_sns == RBT_SNS_L_LEFT) {
                rbt_mov_state = RBT_MOV_TRN_RIGHT_30DEG;
            }

            else {
                rbt_mov_state = RBT_MOV_STP;
            }

        }

        chk_2ms = get_time_2ms();
        if (buf_chk_2ms != chk_2ms) {
            buf_chk_2ms = chk_2ms;

            if (rbt_mov_state == RBT_MOV_TRN_RIGHT_30DEG) {
                outp(LWH, buf_lwh);
                buf_lwh = rotate_r(buf_lwh);                
            }
        }
    }

    return 0;
}

int unittest_ltr_predict_next_state_by_predef(st_line_tracer_state_t* cur_state, uint8_t nxt_sensor) {

    uint8_t buf_rwh = 0x99;
    uint8_t buf_lwh = 0x99;

    uint8_t buf_lwh_ctr;
    uint8_t buf_rwh_ctr;

    unsigned int chk_1ms;
    unsigned int buf_chk_1ms;
    unsigned int chk_1s;
    unsigned int buf_chk_1s;
    unsigned int ctr_chk_1s = 0;

    bool_t next_state_found = False;
    bool_t next_state_find  = False;

    st_line_tracer_state_t nxt_state;
    st_line_tracer_state_t prv_state;

    /* Get previous, current, and next state */
    st_line_tracer_state_t *l_line_tracer_cur_state = cur_state;
    st_line_tracer_state_t *l_line_tracer_nxt_state = &nxt_state;
    st_line_tracer_state_t *l_line_tracer_prv_state = &prv_state;

    f_init_robot();
    reset_timers();

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    buf_lwh_ctr = l_line_tracer_cur_state->rbt_speed_timer_l_wheel;
    buf_rwh_ctr = l_line_tracer_cur_state->rbt_speed_timer_r_wheel;

    FOREVER {

        /* Process wheel movement based on current speed */
        chk_1ms = get_time_1ms();
        if (buf_chk_1ms != chk_1ms) {
            buf_chk_1ms = chk_1ms;
            buf_lwh_ctr--;
            buf_rwh_ctr--;
            if (l_line_tracer_cur_state->rbt_movement_state != RBT_MOV_STP) l_line_tracer_cur_state->rbt_period_ms_ctr++;
        }

        if (buf_lwh_ctr <= 0) {
            
            if (l_line_tracer_cur_state->rbt_speed_timer_l_wheel != 0) {
                buf_lwh = rotate_r(buf_lwh);
                outp(LWH, buf_lwh);
            }

            buf_lwh_ctr = l_line_tracer_cur_state->rbt_speed_timer_l_wheel;
        }

        if (buf_rwh_ctr <= 0) {
            
            if (l_line_tracer_cur_state->rbt_speed_timer_r_wheel != 0) {
                buf_rwh = rotate_l(buf_rwh);
                outp(RWH, buf_rwh);
            }

            buf_rwh_ctr = l_line_tracer_cur_state->rbt_speed_timer_r_wheel;
        }

        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
            ctr_chk_1s++;
        }

        /* Let next state run for 10 secs */
        if (ctr_chk_1s >= 1 && next_state_found) {
            break;
        } 
        /* Let current state run for 10 secs */
        if (ctr_chk_1s >= 1 && !next_state_find) {
            next_state_find = True;
            ctr_chk_1s = 0;
        } 

        /* Predict next state and change state if the condition is met */
        if (f_predict_next_state(l_line_tracer_cur_state, 
                                 l_line_tracer_nxt_state, 
                                 l_line_tracer_prv_state,
                                 nxt_sensor) && !next_state_found && next_state_find) 
        {
            f_change_state(&l_line_tracer_cur_state, 
                           &l_line_tracer_nxt_state, 
                           &l_line_tracer_prv_state);
            next_state_found = True;
        }
    }

    f_done_state(l_line_tracer_cur_state);

    return 0;
}

int unittest_ltr_predict_next_state_by_sensor() {

    uint8_t buf_rwh = 0x99;
    uint8_t buf_lwh = 0x99;

    uint8_t buf_lwh_ctr;
    uint8_t buf_rwh_ctr;

    unsigned int chk_1ms;
    unsigned int buf_chk_1ms;
    unsigned int chk_1s;
    unsigned int buf_chk_1s;
    unsigned int ctr_chk_1s = 0;

    uint8_t cur_sns;

    /* Get previous, current, and next state */
    st_line_tracer_state_t *l_line_tracer_cur_state = f_get_current_state();
    st_line_tracer_state_t *l_line_tracer_nxt_state = f_get_next_state();
    st_line_tracer_state_t *l_line_tracer_prv_state = f_get_previous_state();

    f_init_robot();
    reset_timers();

    chk_1s = get_time_1s();
    buf_chk_1s = chk_1s;

    buf_lwh_ctr = l_line_tracer_cur_state->rbt_speed_timer_l_wheel;
    buf_rwh_ctr = l_line_tracer_cur_state->rbt_speed_timer_r_wheel;

    FOREVER {

        /* Process wheel movement based on current speed */
        chk_1ms = get_time_1ms();
        if (buf_chk_1ms != chk_1ms) {
            buf_chk_1ms = chk_1ms;
            buf_lwh_ctr--;
            buf_rwh_ctr--;
            if (l_line_tracer_cur_state->rbt_movement_state != RBT_MOV_STP) l_line_tracer_cur_state->rbt_period_ms_ctr++;
        }

        if (buf_lwh_ctr <= 0) {
            
            if (l_line_tracer_cur_state->rbt_speed_timer_l_wheel != 0) {
                buf_lwh = rotate_r(buf_lwh);
                outp(LWH, buf_lwh);
            }

            buf_lwh_ctr = l_line_tracer_cur_state->rbt_speed_timer_l_wheel;
        }

        if (buf_rwh_ctr <= 0) {
            
            if (l_line_tracer_cur_state->rbt_speed_timer_r_wheel != 0) {
                buf_rwh = rotate_l(buf_rwh);
                outp(RWH, buf_rwh);
            }

            buf_rwh_ctr = l_line_tracer_cur_state->rbt_speed_timer_r_wheel;
        }

        chk_1s = get_time_1s();
        if (buf_chk_1s != chk_1s) {
            buf_chk_1s = chk_1s;
            ctr_chk_1s++;
        }

        /* Let next state run for 10 secs */
        if (ctr_chk_1s >= 10) {
            break;
        } 

        /* Predict next state and change state if the condition is met */
        cur_sns = get_sns();
        if (f_predict_next_state(l_line_tracer_cur_state, 
                                 l_line_tracer_nxt_state, 
                                 l_line_tracer_prv_state,
                                 cur_sns)) 
        {
            f_change_state(&l_line_tracer_cur_state, 
                           &l_line_tracer_nxt_state, 
                           &l_line_tracer_prv_state);
        }
    }

    f_done_state(l_line_tracer_cur_state);

    return 0;
}
