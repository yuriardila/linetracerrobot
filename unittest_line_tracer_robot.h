/*******************************************************************************
 *
 *      システム名：　ラインとレーサーロボット
 *
 *		Copyright (c) Yuri Ardila 2015
 *
 *-----------------------------------------------------------------------------
 *
 *		$RCSfile: unittest_line_tracer_robot.h,v $		$Revision: 1.0 $
 *
 *		$設　計: Yuri Ardila			$日付: 2015/06/06
 *		$Author: Yuri Ardila $		$Date: 2015-06-06 17:08:02+09 $
 *
 *******************************************************************************/

#ifndef _UNITTEST_H
#define _UNITTEST_H

#include "line_tracer_robot.h"

int unittest_ltr_move_l_wheel();
int unittest_ltr_move_r_wheel();
int unittest_ltr_move_l_wheel_with_period(int period);
int unittest_ltr_move_r_wheel_with_period(int period);
int unittest_ltr_move_both_wheel();
int unittest_ltr_move_both_wheel_with_period(int period);

int unittest_ltr_check_lcd(char* str);
int unittest_ltr_check_lcd_counter1s();

int unittest_ltr_check_sensor();

int unittest_ltr_move_on_sns_middle();
int unittest_ltr_move_on_sns_s_left();
int unittest_ltr_move_on_sns_m_left();
int unittest_ltr_move_on_sns_l_left();
int unittest_ltr_move_on_sns_s_right();
int unittest_ltr_move_on_sns_m_right();
int unittest_ltr_move_on_sns_l_right();

int unittest_ltr_predict_next_state_by_predef(st_line_tracer_state_t* cur_state, uint8_t nxt_sensor);
int unittest_ltr_predict_next_state_by_sensor();

#endif 
/* _UNITTEST_H */